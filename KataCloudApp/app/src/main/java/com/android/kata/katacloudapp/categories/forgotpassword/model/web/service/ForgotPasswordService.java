package com.android.kata.katacloudapp.categories.forgotpassword.model.web.service;

import android.content.Context;

public interface ForgotPasswordService {

    interface OnSuccessResponse {
        void onSuccessListener();
    }

    interface OnErrorResponse {
        void onErrorListener();
    }

    void doForgotPassword(Context context, String email, OnSuccessResponse onSuccessResponse,
                          OnErrorResponse onErrorResponse);
}
