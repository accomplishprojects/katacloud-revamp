package com.android.kata.katacloudapp.categories.registration.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.baseactivity.BaseActivity;
import com.android.kata.katacloudapp.categories.registration.presenter.RegisterContract;
import com.android.kata.katacloudapp.categories.registration.presenter.RegisterContractImpl;
import com.android.kata.katacloudapp.categories.splashscreen.views.SplashActivity;

public class RegistrationActivity extends BaseActivity implements View.OnClickListener, RegisterContract.RegisterView {

    String strEmail, strFirstname, strLastname, strPassword;
    EditText edtEmail, edtFirstname, edtLastname, edtPassword;
    Button btnRegister;
    RegisterContract.RegisterPresenter registerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        setupViews();

        registerPresenter = new RegisterContractImpl(this);
    }

    private void setupViews() {
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtFirstname = (EditText) findViewById(R.id.edtFirstname);
        edtLastname = (EditText) findViewById(R.id.edtLastname);
        edtPassword = (EditText) findViewById(R.id.edtPassword);

        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:
                strEmail = edtEmail.getText().toString();
                strFirstname = edtFirstname.getText().toString();
                strLastname = edtLastname.getText().toString();
                strPassword = edtPassword.getText().toString();
                registerPresenter.registerListener(this, strEmail, strFirstname, strLastname, strPassword);
                break;

        }
    }

    @Override
    public void registerSuccessCallback() {
        startActivity(new Intent(this, SplashActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void registerErrorCallback() {
        Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
    }
}
