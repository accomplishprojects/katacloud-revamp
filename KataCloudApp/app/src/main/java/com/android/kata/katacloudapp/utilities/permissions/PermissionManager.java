package com.android.kata.katacloudapp.utilities.permissions;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.android.kata.katacloudapp.utilities.constants.Constants;

public class PermissionManager {

    String TAG = this.getClass().getSimpleName();
    private static PermissionManager sPermissionManager;

    public static void init() {
        if (sPermissionManager == null) {
            sPermissionManager = new PermissionManager();
        }
    }

    public static PermissionManager getPermissionManagerInstance() {
        if (sPermissionManager != null) {
            return sPermissionManager;
        }
        throw new IllegalStateException("Call Permission Manager init() method");
    }

    public boolean isExternalStorageAllowed(Context context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    public void askPermission(Context context) {
        if (isExternalStorageAllowed(context)) {
            Log.e(TAG, "askExternalStoragePermission: " + " External storage allowed");
        } else {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        Constants.PERMISSION_REQUESTCODE);
            }

        }
    }

}
