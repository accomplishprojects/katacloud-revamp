package com.android.kata.katacloudapp.utilities.userinterface;

import android.app.ProgressDialog;

class CustomUI {

    private static CustomUI sCustomUI;
    private ProgressDialog mProgressDialog;

    public static void init() {
        if (sCustomUI == null) {
            sCustomUI = new CustomUI();
        }
    }

    public static CustomUI getCustomUIInstance() {
        if (sCustomUI != null) {
            return sCustomUI;
        }

        throw new IllegalStateException("Call CustomUI init() method");
    }

    public void showProgressLoader() {

    }
}
