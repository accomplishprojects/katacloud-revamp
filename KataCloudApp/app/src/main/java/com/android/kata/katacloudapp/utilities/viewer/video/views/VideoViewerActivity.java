package com.android.kata.katacloudapp.utilities.viewer.video.views;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.MediaController;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.baseactivity.BaseActivity;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;
import com.google.android.exoplayer.AspectRatioFrameLayout;
import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.MediaCodecSelector;
import com.google.android.exoplayer.MediaCodecTrackRenderer;
import com.google.android.exoplayer.MediaCodecVideoTrackRenderer;
import com.google.android.exoplayer.audio.AudioCapabilities;
import com.google.android.exoplayer.audio.AudioCapabilitiesReceiver;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.upstream.Allocator;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer.util.PlayerControl;
import com.google.android.exoplayer.util.Util;

public class VideoViewerActivity extends BaseActivity implements AudioCapabilitiesReceiver.Listener,
        MediaCodecVideoTrackRenderer.EventListener, ExoPlayer.Listener, View.OnClickListener {

    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    /**
     * ExoPlayer Views
     */
    FrameLayout frameLayout;
    AspectRatioFrameLayout aspectRatioFrameLayout;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;
    Handler mHandler;
    ExoPlayer player;
    PlayerControl playerControl;
    MediaController mediaController;

    private static final int BUFFER_SEGMENT_SIZE = 64 * 1024;
    private static final int BUFFER_SEGMENT_COUNT = 256;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_viewer);
        SharedPreferenceManager.init(this);
        setupViews();
        setupExoplayer();

    }

    private void setupViews() {
        /**Setting up toolbar*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /**Setting up ExoPlayer Views*/
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        frameLayout.setOnClickListener(this);
        aspectRatioFrameLayout = (AspectRatioFrameLayout) findViewById(R.id.aspectRatioFrameLayout);
        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        surfaceHolder = surfaceView.getHolder();
    }


    private void setupExoplayer() {
        mHandler = new Handler();
        player = ExoPlayer.Factory.newInstance(4);
        playerControl = new PlayerControl(player);
        mediaController = new MediaController(this);
        mediaController.setAnchorView(frameLayout);

        /**Getting string details for the view*/
        String userAgent = Util.getUserAgent(this, "KataCloudApp");
        String token = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTokenFromShared();
        String tenantId = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTenantIdFromShared();
        String strFileItemName = getIntent().getStringExtra(Constants.INTENT_FILEPATH_KEY);
        String strFileItemObjectName = getIntent().getStringExtra(Constants.INTENT_FILEOBJECTNAME_KEY);
        String strFileItemContainer = getIntent().getStringExtra(Constants.INTENT_CONTAINER_KEY);
        getSupportActionBar().setTitle(strFileItemObjectName);

        String url = Constants.API_BASE_URL_1 + "AUTH_" + tenantId + "/" + strFileItemContainer + "/" +
                strFileItemName;

        Uri fileItemUri = Uri.parse(url);

        Allocator allocator = new DefaultAllocator(BUFFER_SEGMENT_SIZE);
        DefaultHttpDataSource defaultHttpDataSource = new DefaultHttpDataSource(userAgent, null);
        defaultHttpDataSource.setRequestProperty(Constants.API_AUTHENTICATION_TOKEN_TAG, token);
        defaultHttpDataSource.setRequestProperty(Constants.API_CONTENT_TYPE_TAG, "application/octet-stream");

        ExtractorSampleSource sampleSource = new ExtractorSampleSource(fileItemUri, defaultHttpDataSource, allocator,
                BUFFER_SEGMENT_COUNT * BUFFER_SEGMENT_SIZE, mHandler, null, 0);

        MediaCodecVideoTrackRenderer videoRenderer = new MediaCodecVideoTrackRenderer(
                VideoViewerActivity.this, sampleSource, MediaCodecSelector.DEFAULT,
                MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT, 5000, mHandler, this, 50);

        MediaCodecAudioTrackRenderer audioRenderer = new MediaCodecAudioTrackRenderer(
                sampleSource, MediaCodecSelector.DEFAULT);

        player.prepare(videoRenderer, audioRenderer);
        player.sendMessage(videoRenderer, MediaCodecVideoTrackRenderer.MSG_SET_SURFACE,
                surfaceView.getHolder().getSurface());

        mediaController.setMediaPlayer(playerControl);
        mediaController.setEnabled(true);
        player.addListener(this);
        player.setPlayWhenReady(true);
    }

    private void toggleControlsVisibility() {
        if (mediaController.isShowing()) {
            mediaController.hide();
        } else {
            showControls();
        }
    }

    private void showControls() {
        mediaController.show(0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAudioCapabilitiesChanged(AudioCapabilities audioCapabilities) {
        Log.e(TAG, "onAudioCapabilitiesChanged: ");
        releasePlayer();
    }

    @Override
    public void onDroppedFrames(int count, long elapsed) {
        Log.e(TAG, "onDroppedFrames: ");
    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees,
                                   float pixelWidthHeightRatio) {
        aspectRatioFrameLayout.setAspectRatio(height == 0 ? 1 : (width * pixelWidthHeightRatio) / height);
    }

    @Override
    public void onDrawnToSurface(Surface surface) {
        Log.e(TAG, "onDrawnToSurface: ");
    }

    @Override
    public void onDecoderInitializationError(MediaCodecTrackRenderer.DecoderInitializationException e) {
        Log.e(TAG, "onDecoderInitializationError: ");
    }

    @Override
    public void onCryptoError(MediaCodec.CryptoException e) {
        Log.e(TAG, "onCryptoError: ");
    }

    @Override
    public void onDecoderInitialized(String decoderName, long elapsedRealtimeMs, long initializationDurationMs) {
        Log.e(TAG, "onDecoderInitialized: ");
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        Log.e(TAG, "onPlayerStateChanged: ");
    }

    @Override
    public void onPlayWhenReadyCommitted() {
        Log.e(TAG, "onPlayWhenReadyCommitted: ");
    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Log.e(TAG, "onPlayerError: ");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.frameLayout:
                toggleControlsVisibility();
                break;
        }
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        surfaceView = null;
        releasePlayer();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
