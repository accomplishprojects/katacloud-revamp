package com.android.kata.katacloudapp.categories.forgotpassword.presenter;

import android.content.Context;

import com.android.kata.katacloudapp.categories.forgotpassword.model.web.service.ForgotPasswordService;
import com.android.kata.katacloudapp.categories.forgotpassword.model.web.service.ForgotPasswordServiceImpl;

public class ForgotPasswordContractImpl implements ForgotPasswordContract.ForgotPasswordPresenter,
        ForgotPasswordService.OnSuccessResponse, ForgotPasswordService.OnErrorResponse {

    ForgotPasswordContract.ForgotPasswordView forgotPasswordView;
    ForgotPasswordService forgotPasswordService;

    public ForgotPasswordContractImpl(ForgotPasswordContract.ForgotPasswordView view) {
        forgotPasswordView = view;
        forgotPasswordService = new ForgotPasswordServiceImpl();
    }

    @Override
    public void onForgotPasswordListener(Context context, String email) {
        forgotPasswordService.doForgotPassword(context, email, this, this);
    }

    @Override
    public void onErrorListener() {

    }

    @Override
    public void onSuccessListener() {

    }
}
