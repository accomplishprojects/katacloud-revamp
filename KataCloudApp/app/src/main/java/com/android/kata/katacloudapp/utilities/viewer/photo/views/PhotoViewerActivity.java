package com.android.kata.katacloudapp.utilities.viewer.photo.views;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.baseactivity.BaseActivity;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.database.greendao.db.FileItems;
import com.android.kata.katacloudapp.utilities.viewer.photo.adapters.PhotoViewerAdapter;

import java.util.ArrayList;

public class PhotoViewerActivity extends BaseActivity {

    private String TAG = PhotoViewerFragment.class.getSimpleName();
    private ArrayList<FileItems> fileItemsArrayList;
    private ViewPager viewPager;
    PhotoViewerAdapter photoViewerAdapter;

    TextView txtPositionCounter;
    int selectedPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_viewer);
        setupViews();
        setCurrentItem(selectedPosition);
    }

    private void setupViews() {
        /**Setting up Toolbar*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle fileItemBundle = getIntent().getBundleExtra(Constants.BUNDLE_PHOTO_VIEWER_KEY);

        /**Get data bundle*/
        fileItemsArrayList = (ArrayList<FileItems>) fileItemBundle
                .getSerializable(Constants.BUNDLE_FILE_ITEM_OBJECT_KEY);
        selectedPosition = fileItemBundle.getInt(Constants.BUNDLE_FILE_ITEM_POSITION_KEY);
        Log.e(TAG, Constants.HASHMAP_ORIGINAL_POSITION_TAG + ": " + selectedPosition);
        Log.e(TAG, "ArrayList Sizes: " + fileItemsArrayList.size());

        txtPositionCounter = (TextView) findViewById(R.id.txtPositionCounter);

        /**Setting up ViewPager*/
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        photoViewerAdapter = new PhotoViewerAdapter(this, fileItemsArrayList);
        viewPager.setAdapter(photoViewerAdapter);
        viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                displayMetaInfo(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
    }

    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position, false);
        displayMetaInfo(selectedPosition);
    }

    private void displayMetaInfo(int position) {
        txtPositionCounter.setText((position + 1) + " of " + fileItemsArrayList.size());
        getSupportActionBar().setTitle(fileItemsArrayList.get(position).getObject_name());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
