package com.android.kata.katacloudapp.utilities.notification;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.android.kata.katacloudapp.categories.dashboard.model.Download;
import com.android.kata.katacloudapp.utilities.converters.DataConverters;

import static android.content.Context.NOTIFICATION_SERVICE;

public class CustomNotificationManager {

    private static NotificationManager notificationManager;
    private static NotificationCompat.Builder mNotificatioBuilder;
    private static CustomNotificationManager sCustomNotificationManager;


    public static void init(Context context) {
        if (sCustomNotificationManager == null) {
            sCustomNotificationManager = new CustomNotificationManager();
            mNotificatioBuilder = new NotificationCompat.Builder(context);
            notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        }
    }

    public static CustomNotificationManager getsNotificationManagerInstance() {
        if (sCustomNotificationManager == null) {
            throw new IllegalStateException("Call CustomNotificationManager init()");
        }
        return sCustomNotificationManager;
    }

    public void onShowNotification(int smallIcon, String title, String message, int notificationId) {
        mNotificatioBuilder.setSmallIcon(smallIcon)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true);

        notificationManager.notify(notificationId, mNotificatioBuilder.build());
    }

    public void onDownloadingFile(int notificationId, int percentage, String title) {
        mNotificatioBuilder.setProgress(100, percentage, false);
        mNotificatioBuilder.setContentTitle(title);
        mNotificatioBuilder.setContentText(percentage + "%");
        notificationManager.notify(notificationId, mNotificatioBuilder.build());
    }

    public void onDownloadCompleted(int smallIcon, int notificationId, String title, String message) {
        notificationManager.cancel(0);
        mNotificatioBuilder.setProgress(0, 0, false);
        mNotificatioBuilder.setSmallIcon(smallIcon);
        mNotificatioBuilder.setContentTitle(title);
        mNotificatioBuilder.setContentText(message);
        notificationManager.notify(notificationId, mNotificatioBuilder.build());
    }

    public void onDownloadFailed(int smallIcon, int notificationId, String title, String message) {
        notificationManager.cancel(0);
        mNotificatioBuilder.setProgress(0, 0, false);
        mNotificatioBuilder.setSmallIcon(smallIcon);
        mNotificatioBuilder.setContentTitle(title);
        mNotificatioBuilder.setContentText(message);
        notificationManager.notify(notificationId, mNotificatioBuilder.build());
    }

    public void onUploadCompleted(int smallIcon, int notificationId, String message) {
        notificationManager.cancel(0);
        mNotificatioBuilder.setProgress(0, 0, false);
        mNotificatioBuilder.setSmallIcon(smallIcon);
        mNotificatioBuilder.setContentText(message);
        notificationManager.notify(notificationId, mNotificatioBuilder.build());
    }
}
