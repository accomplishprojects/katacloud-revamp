package com.android.kata.katacloudapp.categories.registration.presenter;

import android.content.Context;

import com.android.kata.katacloudapp.categories.registration.model.web.service.RegisterService;
import com.android.kata.katacloudapp.categories.registration.model.web.service.RegisterServiceImpl;

public class RegisterContractImpl implements RegisterContract.RegisterPresenter,
        RegisterService.OnFinishedValidation {

    RegisterContract.RegisterView registerView;
    RegisterService registerService;

    public RegisterContractImpl(RegisterContract.RegisterView view) {
        registerView = view;
        registerService = new RegisterServiceImpl();
    }

    @Override
    public void registerListener(Context context, String email, String firstname, String lastname, String password) {
        registerService.doRegister(context, this, email, firstname, lastname, password);
    }

    @Override
    public void onSuccess() {
        registerView.registerSuccessCallback();
    }

    @Override
    public void onError() {
        registerView.registerErrorCallback();
    }
}
