package com.android.kata.katacloudapp.categories.registration.presenter;

import android.content.Context;

public interface RegisterContract {

    interface RegisterPresenter {
        void registerListener(Context context, String email, String firstname, String lastname, String password);
    }

    interface RegisterView {
        void registerSuccessCallback();

        void registerErrorCallback();
    }
}
