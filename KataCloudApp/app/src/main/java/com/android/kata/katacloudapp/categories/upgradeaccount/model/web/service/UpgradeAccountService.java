package com.android.kata.katacloudapp.categories.upgradeaccount.model.web.service;

import android.content.Context;

public interface UpgradeAccountService {

    interface OnSuccessResponse {
        void onSuccess();
    }

    interface OnErrorResponse {
        void onError();
    }

    void onValidateVoucherCode(Context context, String voucherCode, OnSuccessResponse onSuccessResponse,
                               OnErrorResponse onErrorResponse);

    void onSubscribe(Context context, OnSuccessResponse onSuccessResponse,
                     OnErrorResponse onErrorResponse);
}
