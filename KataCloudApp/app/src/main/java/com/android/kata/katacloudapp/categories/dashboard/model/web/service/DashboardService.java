package com.android.kata.katacloudapp.categories.dashboard.model.web.service;

import android.content.Context;

public interface DashboardService {

    interface OnSuccessResponse {
        void onSuccess(String actionTag);
    }

    interface OnErrorResponse {
        void onError();
    }

    void onCreateObjectToServer(Context context, String filepath, String container, String actionTag,
                                OnSuccessResponse onSuccessResponse, OnErrorResponse onErrorResponse);

    void onDeleteObjectToServer(Context context, String filepath, String container, String actionTag,
                                OnSuccessResponse onSuccessResponse, OnErrorResponse onErrorResponse);

}
