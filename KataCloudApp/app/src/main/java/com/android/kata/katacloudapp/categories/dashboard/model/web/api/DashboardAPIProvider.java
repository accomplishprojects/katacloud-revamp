package com.android.kata.katacloudapp.categories.dashboard.model.web.api;

import com.android.kata.katacloudapp.utilities.network.RestManager;

import retrofit2.Retrofit;

public class DashboardAPIProvider {

    public static DashboardAPIRoutes getDashboardAPIRoutes() {
        Retrofit mRetrofit = RestManager.getRestManagerInstance().getRetrofitAdapter1();
        return mRetrofit.create(DashboardAPIRoutes.class);
    }
}
