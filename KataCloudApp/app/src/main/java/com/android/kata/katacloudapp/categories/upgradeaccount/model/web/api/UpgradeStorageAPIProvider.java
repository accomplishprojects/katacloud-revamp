package com.android.kata.katacloudapp.categories.upgradeaccount.model.web.api;

import com.android.kata.katacloudapp.utilities.network.RestManager;

import retrofit2.Retrofit;

public class UpgradeStorageAPIProvider {

    public static UpgradeStorageAPIRoutes getUpgradeStorageAPIRoutes() {
        Retrofit mRetrofit = RestManager.getRestManagerInstance().getRetrofitAdapter3();
        return mRetrofit.create(UpgradeStorageAPIRoutes.class);
    }
}
