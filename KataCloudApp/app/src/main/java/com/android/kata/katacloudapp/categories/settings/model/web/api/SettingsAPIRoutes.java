package com.android.kata.katacloudapp.categories.settings.model.web.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface SettingsAPIRoutes {

    @GET("AUTH_{tenantId}")
    Call<ResponseBody> onGetSettings(@Header("X-Auth-Token") String token,
                                     @Path("tenantId") String tenantId);
}
