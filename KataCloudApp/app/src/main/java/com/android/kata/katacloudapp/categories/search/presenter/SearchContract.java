package com.android.kata.katacloudapp.categories.search.presenter;

public interface SearchContract {

    interface SearchView {
        void onFolderSelectedListener(String filepath, int depth, String container);
        void onFileSelectedListener(String container, int position);
    }
}
