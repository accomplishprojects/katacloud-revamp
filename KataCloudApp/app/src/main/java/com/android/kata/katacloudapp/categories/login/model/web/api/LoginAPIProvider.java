package com.android.kata.katacloudapp.categories.login.model.web.api;

import com.android.kata.katacloudapp.utilities.network.RestManager;

import retrofit2.Retrofit;

public class LoginAPIProvider {

    public static LoginAPIRoutes getLoginAPIRoutes() {
        Retrofit mRetrofit = RestManager.getRestManagerInstance().getRetrofitAdapter2();
        return mRetrofit.create(LoginAPIRoutes.class);
    }
}
