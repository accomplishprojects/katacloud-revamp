package com.android.kata.katacloudapp.categories.dashboard.model.androidcomponents.asynctask;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.android.kata.katacloudapp.utilities.checkers.CheckersUtils;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.database.greendao.db.FileItems;
import com.android.kata.katacloudapp.utilities.database.greendao.db.GreenDaoManager;

import java.util.List;

import retrofit2.Response;

public class PopulateLocalDBTask extends AsyncTask<Void, Void, Void> {

    private Response<List<FileItems>> response;
    private String TAG = this.getClass().getSimpleName();
    private String tenantId, container;
    private Context context;

    public PopulateLocalDBTask(Context context, Response<List<FileItems>> response,
                               String tenantId, String container) {
        this.context = context;
        this.response = response;
        this.tenantId = tenantId;
        this.container = container;
    }

    @Override
    protected Void doInBackground(Void... voids) {

        if (response.body() != null) {

            for (FileItems items : response.body()) {
                /**Insert response into greendao db*/
                FileItems fileItems = new FileItems();
                fileItems.setTenant_id(tenantId);
                fileItems.setHash_code(items.getHash_code());
                fileItems.setObject_name(CheckersUtils.getCheckersUtilsInstance()
                        .getFileItemObjectName(items.getName()));
                fileItems.setBytes(items.getBytes());
                fileItems.setName(items.getName());
                fileItems.setContainer(container);
                fileItems.setDepth(CheckersUtils.getCheckersUtilsInstance().
                        getFileItemDepth(items.getName()));


                /**Insert FileItemObject into Greendao DB*/
                if (!items.getName().contains("thumbNail4FC"))
                    GreenDaoManager.getGreenDaoManagerInstance().getDaoSession().insert(fileItems);
                Log.e(TAG, "onResponse: " + items.getName()
                        + " ObjectName: " + CheckersUtils.getCheckersUtilsInstance().getFileItemObjectName(items.getName())
                        + " Depths: " + CheckersUtils.getCheckersUtilsInstance().getFileItemDepth(items.getName())
                        + " Container: " + container);
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        /**Sending broadcast response to DashboardActivity.java*/
        context.sendBroadcast(new Intent()
                .putExtra(Constants.INTENT_CONTAINER_KEY, container)
                .putExtra(Constants.INTENT_ACTIONTAG_KEY, Constants.ACTION_SYNCHRONIZE_TAG)
                .setAction(Constants.SYNCHRONIZATION_RECEIVER));

    }
}
