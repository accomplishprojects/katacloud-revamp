package com.android.kata.katacloudapp.categories.login.presenter;

import android.content.Context;

public interface LoginContract {

    interface LoginPresenter {
        void loginListener(Context context, String username, String password);
    }

    interface LoginView {
        void loginSuccessCallback();
        void loginErrorCallback();
    }
}
