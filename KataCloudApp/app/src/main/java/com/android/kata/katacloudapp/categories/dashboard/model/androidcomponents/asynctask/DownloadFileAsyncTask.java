package com.android.kata.katacloudapp.categories.dashboard.model.androidcomponents.asynctask;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.android.kata.katacloudapp.utilities.constants.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DownloadFileAsyncTask extends AsyncTask<InputStream, Void, Boolean> {

    private String TAG = "Luigi";
    private File file;
    private boolean isExternalDirectoryCreated = false;
    private String filename;
    private Context context;


    public DownloadFileAsyncTask(Context context, String filename, String container) {
        this.context = context;
        this.filename = filename;

        String externalStorageDirectoryPath = Constants.EXTERNAL_STORAGE_DOWNLOAD_PATH + container + "/";
        file = new File(externalStorageDirectoryPath);
        isExternalDirectoryCreated = file.exists();

        if (!isExternalDirectoryCreated) {
            isExternalDirectoryCreated = file.mkdirs();
            if (isExternalDirectoryCreated) {
                file = new File(externalStorageDirectoryPath, filename);
            }
        } else {
            file = new File(externalStorageDirectoryPath, filename);
        }

    }


    @Override
    protected Boolean doInBackground(InputStream... params) {
        InputStream inputStream = params[0];
        OutputStream output = null;

        try {
            output = new FileOutputStream(file);

            byte[] buffer = new byte[1024];
            int read;

            while ((read = inputStream.read(buffer)) != -1) {

                output.write(buffer, 0, read);
                Log.v(TAG, "Writing to buffer to output stream.");
            }
            Log.d(TAG, "Flushing output stream.");
            output.flush();
            Log.d(TAG, "Output flushed.");
        } catch (IOException e) {
            Log.e(TAG, "IO Exception: " + e.getMessage());
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (output != null) {
                    output.close();
                    Log.d(TAG, "Output stream closed sucessfully.");
                } else {
                    Log.d(TAG, "Output stream is null");
                }
            } catch (IOException e) {
                Log.e(TAG, "Couldn't close output stream: " + e.getMessage());
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        Log.d(TAG, "Downloading file status: " + result);
        if (result) {
            Toast.makeText(this.context, "Downloading " + filename + " successful",
                    Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(this.context, "Downloading " + filename + " failed",
                    Toast.LENGTH_SHORT).show();
        }
    }

}