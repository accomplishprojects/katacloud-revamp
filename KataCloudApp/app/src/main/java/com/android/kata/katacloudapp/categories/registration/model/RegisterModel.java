package com.android.kata.katacloudapp.categories.registration.model;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterModel {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("tenantid")
    @Expose
    private String tenantid;
    @SerializedName("tenantname")
    @Expose
    private String tenantname;
    @SerializedName("uploadstatus")
    @Expose
    private Integer uploadstatus;
    @SerializedName("tokenexp")
    @Expose
    private String tokenexp;
    @SerializedName("DateCreated")
    @Expose
    private String dateCreated;
    @SerializedName("DateLastSyncOn")
    @Expose
    private String dateLastSyncOn;

    /**
     * @return The token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token The token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return The tenantid
     */
    public String getTenantid() {
        return tenantid;
    }

    /**
     * @param tenantid The tenantid
     */
    public void setTenantid(String tenantid) {
        this.tenantid = tenantid;
    }

    /**
     * @return The tenantname
     */
    public String getTenantname() {
        return tenantname;
    }

    /**
     * @param tenantname The tenantname
     */
    public void setTenantname(String tenantname) {
        this.tenantname = tenantname;
    }

    /**
     * @return The uploadstatus
     */
    public Integer getUploadstatus() {
        return uploadstatus;
    }

    /**
     * @param uploadstatus The uploadstatus
     */
    public void setUploadstatus(Integer uploadstatus) {
        this.uploadstatus = uploadstatus;
    }

    /**
     * @return The tokenexp
     */
    public String getTokenexp() {
        return tokenexp;
    }

    /**
     * @param tokenexp The tokenexp
     */
    public void setTokenexp(String tokenexp) {
        this.tokenexp = tokenexp;
    }

    /**
     * @return The dateCreated
     */
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated The DateCreated
     */
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return The dateLastSyncOn
     */
    public String getDateLastSyncOn() {
        return dateLastSyncOn;
    }

    /**
     * @param dateLastSyncOn The DateLastSyncOn
     */
    public void setDateLastSyncOn(String dateLastSyncOn) {
        this.dateLastSyncOn = dateLastSyncOn;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}