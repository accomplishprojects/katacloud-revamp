package com.android.kata.katacloudapp.categories.search.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.baseactivity.BaseActivity;
import com.android.kata.katacloudapp.categories.search.adapter.SearchAdapter;
import com.android.kata.katacloudapp.categories.search.presenter.SearchContract;
import com.android.kata.katacloudapp.utilities.checkers.CheckersUtils;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.database.greendao.db.FileItems;
import com.android.kata.katacloudapp.utilities.database.greendao.db.GreenDaoManager;
import com.android.kata.katacloudapp.utilities.viewer.audio.views.AudioViewerActivity;
import com.android.kata.katacloudapp.utilities.viewer.photo.helperclasses.PhotoViewerHelper;
import com.android.kata.katacloudapp.utilities.viewer.photo.views.PhotoViewerActivity;
import com.android.kata.katacloudapp.utilities.viewer.video.exoplayerhelperclasses.ExoPlayerActivity;
import com.google.android.exoplayer.util.Util;

import java.util.List;

public class SearchActivity extends BaseActivity implements SearchContract.SearchView {

    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    RecyclerView searchListRecyclerView;
    SearchAdapter searchAdapter;
    TextView txtDirectoryPath;
    List<FileItems> fileItemsList;

    /**
     * Search adapter on file pressed
     */
    int containerDepth = 1;
    String container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        GreenDaoManager.init(this);

        /**Setting up views*/
        setupViews();
    }

    private void setupViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        txtDirectoryPath = (TextView) findViewById(R.id.txtDirectoryPath);

        fileItemsList = GreenDaoManager.getGreenDaoManagerInstance().getAllFileItems();
        searchAdapter = new SearchAdapter(this, fileItemsList);
        searchListRecyclerView = (RecyclerView) findViewById(R.id.searchListRecyclerView);
        searchListRecyclerView.setHasFixedSize(true);
        searchListRecyclerView.setLayoutManager(new GridLayoutManager(this, Constants.GRIDVIEW_COLUMN_NUMBER));
        searchListRecyclerView.setAdapter(searchAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /**Setting up search view in toolbar*/
        getMenuInflater().inflate(R.menu.toolbar_menu_search, menu);
        MenuItem menuItemSearch = menu.findItem(R.id.toolbar_menu_search);
        android.support.v7.widget.SearchView toolbarSearch = (android.support.v7.widget.SearchView) MenuItemCompat.getActionView(menuItemSearch);
        toolbarSearch.setIconifiedByDefault(false);
        toolbarSearch.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchAdapter.filter(query.trim());
                searchListRecyclerView.invalidate();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onFolderSelectedListener(String filepath, int depth, String container) {
        Log.e(TAG, "onFolderSelected: " + filepath);
        Log.e(TAG, "onFolderSelected: " + container);
        Log.e(TAG, "onFolderSelected: Depth: " + depth);
        this.containerDepth = depth;
        this.container = container;
        containerDepth++;
        Log.e(TAG, "onFolderSelected: ContainerDepth: " + this.containerDepth);
        txtDirectoryPath.setText(filepath);

        if (fileItemsList != null) {
            fileItemsList.clear();
            fileItemsList.addAll(GreenDaoManager.getGreenDaoManagerInstance()
                    .getDirectoryFiles(filepath, this.containerDepth, container));
            searchAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onFileSelectedListener(String container, int position) {
        String fileItemName = fileItemsList.get(position).getName();
        String fileItemObjectName = fileItemsList.get(position).getObject_name();

        Log.e(TAG, "onFileSelected: " + fileItemName);
        if (CheckersUtils.getCheckersUtilsInstance().checkIfImageFile(fileItemName)) {
            startActivity(new Intent(this, PhotoViewerActivity.class)
                    .putExtra(Constants.BUNDLE_PHOTO_VIEWER_KEY,
                            PhotoViewerHelper.getCustomPhotoViewerInstance()
                                    .getViewableFilesBundle(fileItemsList, position)));
        } else if (CheckersUtils.getCheckersUtilsInstance().checkIfVideoFile(fileItemName)) {
            startActivity(new Intent(this, ExoPlayerActivity.class)
                    .putExtra(Constants.INTENT_FILEPATH_KEY, fileItemName)
                    .putExtra(Constants.INTENT_FILEOBJECTNAME_KEY, fileItemObjectName)
                    .putExtra(Constants.INTENT_CONTAINER_KEY, container)
                    .putExtra(Constants.INTENT_CONTENT_TYPE_EXTRA, Util.TYPE_OTHER));
        } else if (CheckersUtils.getCheckersUtilsInstance().checkIfAudioFile(fileItemName)) {
            startActivity(new Intent(this, AudioViewerActivity.class)
                    .putExtra(Constants.INTENT_FILEPATH_KEY, fileItemName)
                    .putExtra(Constants.INTENT_FILEOBJECTNAME_KEY, fileItemObjectName)
                    .putExtra(Constants.INTENT_CONTAINER_KEY, container)
                    .putExtra(Constants.INTENT_CONTENT_TYPE_EXTRA, Util.TYPE_OTHER));
        }
    }

    @Override
    public void onBackPressed() {
        if (containerDepth > 1) {
            onSearchBackPressed();
        } else {
            super.onBackPressed();
            finish();
        }
    }

    public void onSearchBackPressed() {
        Log.e(TAG, "onBackPressed-1: ContainerDepth: " + containerDepth +
                " Container: " + this.container);
        String filepath = txtDirectoryPath.getText().toString();
        fileItemsList.clear();
        if (containerDepth > 1) {
            containerDepth--;
            if (containerDepth == 1) {
                txtDirectoryPath.setText("");
                fileItemsList.addAll(GreenDaoManager.getGreenDaoManagerInstance().getAllFileItems());
            } else {
                txtDirectoryPath.setText(CheckersUtils.getCheckersUtilsInstance()
                        .removeLastDirectory(filepath));
                fileItemsList.addAll(GreenDaoManager.getGreenDaoManagerInstance()
                        .getDirectoryFiles(txtDirectoryPath.getText().toString(),
                                containerDepth, this.container));
            }
            Log.e(TAG, "onBackPressed-2: ContainerDepth: " + containerDepth +
                    " Container: " + this.container);
        } else {
            txtDirectoryPath.setText("");
            fileItemsList.addAll(GreenDaoManager.getGreenDaoManagerInstance()
                    .getAllFileItems());
        }
        searchAdapter.notifyDataSetChanged();
    }
}
