package com.android.kata.katacloudapp.categories.registration.model.web.api;

import com.android.kata.katacloudapp.categories.registration.model.RegisterModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RegisterAPIRoutes {

    @FormUrlEncoded
    @POST("KataCloud/create_user.php")
    Call<RegisterModel> onRegister(@Field("email") String email,
                                   @Field("firstname") String firstname,
                                   @Field("lastname") String lastname,
                                   @Field("password") String password,
                                   @Field("imei") String imei,
                                   @Field("model") String model,
                                   @Field("serial") String serial,
                                   @Field("autoupload") String autoupload);

}
