package com.android.kata.katacloudapp.utilities.viewer.photo.views;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.database.greendao.db.FileItems;
import com.android.kata.katacloudapp.utilities.viewer.photo.adapters.PhotoViewerAdapter;

import java.util.ArrayList;

public class PhotoViewerFragment extends DialogFragment {
    private String TAG = PhotoViewerFragment.class.getSimpleName();
    private ArrayList<FileItems> fileItemsArrayList;
    private ViewPager viewPager;
    PhotoViewerAdapter photoViewerAdapter;

    TextView txtPositionCounter;
    int selectedPosition = 0;
    View v;

    public static PhotoViewerFragment newInstance() {
        return new PhotoViewerFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.activity_photo_viewer, container, false);
        setupViews();
        setCurrentItem(selectedPosition);
        return v;
    }

    private void setupViews() {
        /**Setting up Toolbar*/
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /**Get data bundle*/
        fileItemsArrayList = (ArrayList<FileItems>) getArguments()
                .getSerializable(Constants.BUNDLE_FILE_ITEM_OBJECT_KEY);
        selectedPosition = getArguments().getInt(Constants.BUNDLE_FILE_ITEM_POSITION_KEY);
        Log.e(TAG, Constants.HASHMAP_ORIGINAL_POSITION_TAG + ": " + selectedPosition);
        Log.e(TAG, "ArrayList Sizes: " + fileItemsArrayList.size());

        txtPositionCounter = (TextView) v.findViewById(R.id.txtPositionCounter);

        /**Setting up ViewPager*/
        viewPager = (ViewPager) v.findViewById(R.id.viewPager);
        photoViewerAdapter = new PhotoViewerAdapter(getActivity(), fileItemsArrayList);
        viewPager.setAdapter(photoViewerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                displayMetaInfo(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
    }

    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position, false);
        displayMetaInfo(selectedPosition);
    }

    private void displayMetaInfo(int position) {
        txtPositionCounter.setText((position + 1) + " of " + fileItemsArrayList.size());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
