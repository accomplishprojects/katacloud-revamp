package com.android.kata.katacloudapp.categories.dashboard.model.androidcomponents.intentservice;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.android.kata.katacloudapp.categories.dashboard.model.androidcomponents.asynctask.PopulateLocalDBTask;
import com.android.kata.katacloudapp.categories.dashboard.model.web.api.DashboardAPIProvider;
import com.android.kata.katacloudapp.utilities.checkers.CheckersUtils;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.database.greendao.db.FileItems;
import com.android.kata.katacloudapp.utilities.database.greendao.db.GreenDaoManager;
import com.android.kata.katacloudapp.utilities.network.RestManager;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MusicIntentService extends IntentService {

    String TAG = this.getClass().getSimpleName();
    String token, tenantId;
    String CONTAINER_TAG = Constants.CONTAINER_MUSIC_TAG;

    public MusicIntentService() {
        super("MusicIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        RestManager.init();
        GreenDaoManager.init(getApplicationContext());
        SharedPreferenceManager.init(getApplicationContext());
        CheckersUtils.init();

        /**Getting user details from shared preferences*/
        token = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTokenFromShared();
        tenantId = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTenantIdFromShared();

        /**Getting folder items of videos from API*/
        Call<List<FileItems>> getContainerFiles = DashboardAPIProvider.getDashboardAPIRoutes()
                .onGetContainerFiles(token, tenantId, CONTAINER_TAG);
        getContainerFiles.enqueue(new Callback<List<FileItems>>() {
            @Override
            public void onResponse(Call<List<FileItems>> call, Response<List<FileItems>> response) {
                Log.e(TAG, "onResponse: " + response.raw());
                new PopulateLocalDBTask(getApplicationContext(), response, tenantId, CONTAINER_TAG).execute();

            }

            @Override
            public void onFailure(Call<List<FileItems>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });

        /**Terminating Service Call*/
        stopSelf();
    }
}
