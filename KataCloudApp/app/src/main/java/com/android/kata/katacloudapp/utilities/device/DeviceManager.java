package com.android.kata.katacloudapp.utilities.device;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;

public class DeviceManager {

    private static DeviceManager sDeviceManager;
    private static TelephonyManager mTelephonyManager;


    public static void init(Context context) {
        if (sDeviceManager == null) {
            sDeviceManager = new DeviceManager();
            mTelephonyManager = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
        }
    }

    public static DeviceManager getDeviceManagerInstance() {
        if (sDeviceManager != null) {
            return sDeviceManager;
        }
        throw new IllegalStateException("Call DeviceManager init()");
    }

    public String getImei() {
        return mTelephonyManager.getDeviceId();
    }

    public String getBrand() {
        return Build.BRAND;
    }

    public String getModel() {
        return Build.MODEL;
    }
}
