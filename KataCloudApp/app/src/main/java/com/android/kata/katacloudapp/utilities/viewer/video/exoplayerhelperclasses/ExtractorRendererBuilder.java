package com.android.kata.katacloudapp.utilities.viewer.video.exoplayerhelperclasses;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaCodec;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import com.android.kata.katacloudapp.utilities.applicationcontroller.ApplicationController;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.MediaCodecSelector;
import com.google.android.exoplayer.MediaCodecVideoTrackRenderer;
import com.google.android.exoplayer.TrackRenderer;
import com.google.android.exoplayer.audio.AudioCapabilities;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.text.TextTrackRenderer;
import com.google.android.exoplayer.upstream.Allocator;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer.upstream.DefaultHttpDataSource;

public class ExtractorRendererBuilder implements DemoPlayer.RendererBuilder {


    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    private final Context context;
    private final String userAgent;
    private final String token;
    private final Uri uri;

    public ExtractorRendererBuilder(Context context, String userAgent, String token, Uri uri) {
        this.context = context;
        this.userAgent = userAgent;
        this.token = token;
//        String proxyUrl = ApplicationController.getProxy(context).getProxyUrl(uri.toString());
//        this.uri = Uri.parse(proxyUrl);
//        Log.e(TAG, "ExtractorRendererBuilder: " + this.uri);
        this.uri = uri;

    }

    @Override
    public void buildRenderers(DemoPlayer player) {
        Allocator allocator = new DefaultAllocator(Constants.BUFFER_SEGMENT_SIZE);
        Handler mainHandler = player.getMainHandler();
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter(mainHandler, null);

        DefaultHttpDataSource defaultHttpDataSource = new DefaultHttpDataSource(userAgent, null);
        defaultHttpDataSource.setRequestProperty(Constants.API_AUTHENTICATION_TOKEN_TAG, token);
        defaultHttpDataSource.setRequestProperty(Constants.API_CONTENT_TYPE_TAG, "application/octet-stream");


        ExtractorSampleSource sampleSource = new ExtractorSampleSource(uri, defaultHttpDataSource, allocator,
                Constants.BUFFER_SEGMENT_COUNT * Constants.BUFFER_SEGMENT_SIZE, mainHandler, player, 0);
        MediaCodecVideoTrackRenderer videoRenderer = new MediaCodecVideoTrackRenderer(context, sampleSource,
                MediaCodecSelector.DEFAULT, MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT, 5000,
                mainHandler, player, 50);
        MediaCodecAudioTrackRenderer audioRenderer = new MediaCodecAudioTrackRenderer(sampleSource,
                MediaCodecSelector.DEFAULT, null, true, mainHandler, player,
                AudioCapabilities.getCapabilities(context), AudioManager.STREAM_MUSIC);

        TrackRenderer textRenderer = new TextTrackRenderer(sampleSource, player, mainHandler.getLooper());
        TrackRenderer[] renderers = new TrackRenderer[DemoPlayer.RENDERER_COUNT];
        renderers[DemoPlayer.TYPE_VIDEO] = videoRenderer;
        renderers[DemoPlayer.TYPE_AUDIO] = audioRenderer;
        renderers[DemoPlayer.TYPE_TEXT] = textRenderer;
        player.onRenderers(renderers, bandwidthMeter);


    }

    @Override
    public void cancel() {
        // Do nothing.
    }
}
