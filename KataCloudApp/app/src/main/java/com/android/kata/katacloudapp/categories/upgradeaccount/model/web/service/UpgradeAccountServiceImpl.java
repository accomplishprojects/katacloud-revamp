package com.android.kata.katacloudapp.categories.upgradeaccount.model.web.service;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.android.kata.katacloudapp.categories.upgradeaccount.model.web.api.UpgradeStorageAPIProvider;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.device.DeviceManager;
import com.android.kata.katacloudapp.utilities.network.RestManager;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpgradeAccountServiceImpl implements UpgradeAccountService {

    /**
     * Flags
     */
    private String TAG = this.getClass().getSimpleName();

    @Override
    public void onValidateVoucherCode(Context context, String voucherCode, OnSuccessResponse onSuccessResponse,
                                      OnErrorResponse onErrorResponse) {
        RestManager.init();
        SharedPreferenceManager.init(context);

        String token = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTokenFromShared();
        String tenantName = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTenantNameFromShared();
        String requestCode = Constants.API_UPGRADE_ACCOUNT_REQUEST_CODE_VALUE;

        Call<ResponseBody> onValidateVoucherCodeCall = UpgradeStorageAPIProvider.getUpgradeStorageAPIRoutes()
                .onValidateVoucherCode(token, tenantName, requestCode, voucherCode, "", "");
        onValidateVoucherCodeCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Log.e(TAG, "onResponse: " + response.raw().toString());
                    Log.e(TAG, "onResponse: " + response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    @Override
    public void onSubscribe(Context context, OnSuccessResponse onSuccessResponse,
                            OnErrorResponse onErrorResponse) {
        RestManager.init();
        SharedPreferenceManager.init(context);
        DeviceManager.init(context);

        String tenantName = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTenantNameFromShared();
        String brand = DeviceManager.getDeviceManagerInstance().getBrand();
        String model = DeviceManager.getDeviceManagerInstance().getModel();
        String imei = DeviceManager.getDeviceManagerInstance().getImei();
        String requestCode = Constants.API_UPGRADE_ACCOUNT_REQUEST_CODE_VALUE;

        Call<ResponseBody> onSubscribeCall = UpgradeStorageAPIProvider.getUpgradeStorageAPIRoutes()
                .onUpgradeStorage(tenantName, requestCode, "", "", "", imei, brand, "", model);
        onSubscribeCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Log.e(TAG, "onResponse: " + response.raw().toString());
                    Log.e(TAG, "onResponse: " + response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
}
