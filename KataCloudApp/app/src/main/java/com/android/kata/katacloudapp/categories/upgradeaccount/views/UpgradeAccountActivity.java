package com.android.kata.katacloudapp.categories.upgradeaccount.views;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.baseactivity.BaseActivity;
import com.android.kata.katacloudapp.categories.upgradeaccount.presenter.UpgradeAccountContract;
import com.android.kata.katacloudapp.categories.upgradeaccount.presenter.UpgradeAccountContractImpl;

public class UpgradeAccountActivity extends BaseActivity implements View.OnClickListener,
        UpgradeAccountContract.UpgradeAccountView {

    /**
     * Presenter Declaration
     */
    UpgradeAccountContract.UpgradeAccountPresenter upgradeAccountPresenter;

    Spinner spnStorageSize;
    EditText edtVoucherCode;
    Button btnAddVoucher, btnSubscribe;
    ListView lvVoucherNo;
    TextView txtPayment, txtAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_upgrade_account);
        setupViews();

        upgradeAccountPresenter = new UpgradeAccountContractImpl(this);
    }

    private void setupViews() {
        /**Set up toolbar*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        spnStorageSize = (Spinner) findViewById(R.id.spnStorageSize);
        edtVoucherCode = (EditText) findViewById(R.id.edtVoucherCode);
        lvVoucherNo = (ListView) findViewById(R.id.lvVoucherNo);
        txtPayment = (TextView) findViewById(R.id.txtPayment);
        txtAmount = (TextView) findViewById(R.id.txtAmount);

        btnAddVoucher = (Button) findViewById(R.id.btnAddVoucher);
        btnSubscribe = (Button) findViewById(R.id.btnSubscribe);
        btnAddVoucher.setOnClickListener(this);
        btnSubscribe.setOnClickListener(this);

        /**Load list of storage sizes to spinner and set spinner action listeners*/
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.spinner_storage_sizes,
                android.R.layout.simple_expandable_list_item_1);
        spnStorageSize.setAdapter(adapter);
        spnStorageSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        txtAmount.setText(getString(R.string.label_25USD));
                        break;

                    case 1:
                        txtAmount.setText(getString(R.string.label_50USD));
                        break;

                    case 2:
                        txtAmount.setText(getString(R.string.label_100USD));
                        break;

                    case 3:
                        txtAmount.setText(getString(R.string.label_200USD));
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddVoucher:
                String voucherCode = edtVoucherCode.getText().toString();
                upgradeAccountPresenter.onValidateCodeListener(this, voucherCode);
                break;

            case R.id.btnSubscribe:
                upgradeAccountPresenter.onSubscribeListener(this);
                break;
        }
    }

    @Override
    public void onSuccessCallback() {

    }

    @Override
    public void onErrorCallback() {

    }
}
