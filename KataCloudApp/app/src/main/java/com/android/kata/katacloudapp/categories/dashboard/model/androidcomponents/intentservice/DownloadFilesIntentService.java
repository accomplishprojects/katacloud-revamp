package com.android.kata.katacloudapp.categories.dashboard.model.androidcomponents.intentservice;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.dashboard.model.Download;
import com.android.kata.katacloudapp.categories.dashboard.model.web.api.DashboardAPIProvider;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.network.RestManager;
import com.android.kata.katacloudapp.utilities.notification.CustomNotificationManager;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class DownloadFilesIntentService extends IntentService {

    String TAG = this.getClass().getSimpleName();
    String externalStorageDirectoryPath;
    String strNotifTitle, strNotifMessage;

    public DownloadFilesIntentService() {
        super("DownloadFilesIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        RestManager.init();
        SharedPreferenceManager.init(getApplicationContext());
        CustomNotificationManager.init(getApplicationContext());

        String actionTag = intent.getStringExtra(Constants.INTENT_ACTIONTAG_KEY);
        String filepath = intent.getStringExtra(Constants.INTENT_FILEPATH_KEY);
        final String fileObjectName = intent.getStringExtra(Constants.INTENT_FILEOBJECTNAME_KEY);
        final String container = intent.getStringExtra(Constants.INTENT_CONTAINER_KEY);
        String token = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTokenFromShared();
        String tenantId = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTenantIdFromShared();

        /**Setting up external directory*/
        externalStorageDirectoryPath = Constants.EXTERNAL_STORAGE_DOWNLOAD_PATH + container + "/";
        File file = new File(externalStorageDirectoryPath);
        checkIfLocalDirectoryExist(file, externalStorageDirectoryPath);

        Call<ResponseBody> onDownloadObjectCall = DashboardAPIProvider.getDashboardAPIRoutes()
                .onDowloadObject(token, tenantId, container, filepath);
        try {
            /**Download file from server*/
            strNotifTitle = "Dowloading " + fileObjectName;
            CustomNotificationManager.getsNotificationManagerInstance()
                    .onDownloadingFile(Constants.NOTIFICATION_DOWNLOAD_TAG, 0, strNotifTitle);

            onDownload(onDownloadObjectCall.execute().body(), container, fileObjectName);
        } catch (IOException e) {
            Log.e(TAG, "onHandleIntent: " + e.getMessage());
            strNotifTitle = "Download Failed";
            strNotifMessage = fileObjectName;
            CustomNotificationManager.getsNotificationManagerInstance()
                    .onDownloadFailed(android.R.drawable.stat_notify_error,
                            Constants.NOTIFICATION_DOWNLOAD_TAG, strNotifTitle, strNotifMessage);
        }

//        onDownloadObjectCall.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if (response.isSuccessful()) {
//                    new DownloadFileAsyncTask(getApplicationContext(), fileObjectName, container)
//                            .execute(response.body().byteStream());
//                } else {
//                    Log.e(TAG, "onResponse: Message: " + response.message() +
//                            " Status code: " + response.code());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Log.e(TAG, "onFailure: " + t.getMessage());
//            }
//        });

        Log.e(TAG, "onHandleIntent: " + actionTag + " : "
                + " Filepath: " + filepath
                + " FileObjectName: " + fileObjectName
                + " Container: " + container);
    }


    private void onDownload(ResponseBody body, String container, String filename) throws IOException {
        /**Show notification*/
        int count;
        byte data[] = new byte[1024 * 4];
        long fileSize = body.contentLength();
        InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);

        File outputFile = new File(externalStorageDirectoryPath, filename);
        OutputStream output = new FileOutputStream(outputFile);
        long total = 0;
        int percetage = 0;

        while ((count = bis.read(data)) != -1) {
            total += count;
            percetage = (int) ((total * 100) / fileSize);

            /**Show notification with progress*/
            strNotifTitle = "Dowloading " + filename;
            CustomNotificationManager.getsNotificationManagerInstance()
                    .onDownloadingFile(Constants.NOTIFICATION_DOWNLOAD_TAG, percetage, strNotifTitle);
            output.write(data, 0, count);
        }

        /**Show download finished notification*/
        stopSelf();
        strNotifTitle = "Download Completed";
        strNotifMessage = filename;
        CustomNotificationManager.getsNotificationManagerInstance()
                .onDownloadCompleted(android.R.drawable.stat_sys_download_done,
                        Constants.NOTIFICATION_DOWNLOAD_TAG, strNotifTitle, strNotifMessage);

        output.flush();
        output.close();
        bis.close();
    }

    public File checkIfLocalDirectoryExist(File file, String externalStorageDirectoryPath) {
        boolean isExternalDirectoryCreated = file.exists();

        if (!isExternalDirectoryCreated) {
            isExternalDirectoryCreated = file.mkdirs();
            if (isExternalDirectoryCreated) {
                file = new File(externalStorageDirectoryPath);
                Log.e(TAG, "checkIfLocalFolderExist: " + "1");
            } else {
                Log.e(TAG, "checkIfLocalFolderExist: " + " Path not created!");
            }
        } else {
            file = new File(externalStorageDirectoryPath);
            Log.e(TAG, "checkIfLocalFolderExist: " + "2");
        }

        Log.e(TAG, "checkIfLocalFolderExist: " + isExternalDirectoryCreated);
        return file;
    }

}
