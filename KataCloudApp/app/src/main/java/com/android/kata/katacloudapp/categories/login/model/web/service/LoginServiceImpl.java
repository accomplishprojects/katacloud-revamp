package com.android.kata.katacloudapp.categories.login.model.web.service;

import android.content.Context;
import android.util.Log;

import com.android.kata.katacloudapp.categories.login.model.LoginModel;
import com.android.kata.katacloudapp.categories.login.model.web.api.LoginAPIProvider;
import com.android.kata.katacloudapp.utilities.network.RestManager;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginServiceImpl implements LoginService {
    String TAG = "Luigi";

    @Override
    public void doLogin(Context context, final OnFinishedValidation listener, String username, String password) {
        RestManager.init();
        SharedPreferenceManager.init(context);

        Call<LoginModel> loginCall = LoginAPIProvider.getLoginAPIRoutes().onLogin("", username, password, "mobile", "3.6");
        loginCall.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {

                String strToken, strTenantId, strTenantName, strUserFirstname, strUserLastname;
                strToken = response.body().getToken();
                strTenantId = response.body().getTenantid();
                strTenantName = response.body().getTenantname();
                strUserFirstname = response.body().getFirstname();
                strUserLastname = response.body().getLastname();

                if (strToken != null && strTenantId != null) {
                    SharedPreferenceManager.getSharedPreferenceManagerInstance()
                            .putLoginDetailsToShared(strToken, strTenantId, strTenantName, strUserFirstname, strUserLastname);
                    listener.onSuccess();
                } else {
                    listener.onError();
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                listener.onError();
            }
        });
    }
}
