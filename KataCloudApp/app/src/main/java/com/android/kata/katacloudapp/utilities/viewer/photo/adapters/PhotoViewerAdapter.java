package com.android.kata.katacloudapp.utilities.viewer.photo.adapters;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.utilities.checkers.CheckersUtils;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.database.greendao.db.FileItems;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

public class PhotoViewerAdapter extends PagerAdapter {

    /**
     * Flags
     */
    private String TAG = this.getClass().getSimpleName();

    private LayoutInflater layoutInflater;
    private Context mContext;
    private ArrayList<FileItems> fileItemsArrayList;

    private ImageView imageViewPreview;

    public PhotoViewerAdapter(Context context, ArrayList<FileItems> fileItemsArrayList) {
        SharedPreferenceManager.init(context);
        CheckersUtils.init();

        mContext = context;
        this.fileItemsArrayList = fileItemsArrayList;
        layoutInflater = (LayoutInflater) mContext.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public final Object instantiateItem(final ViewGroup container, final int position) {
        View view = layoutInflater.inflate(R.layout.image_fullscreen_preview, container, false);
        imageViewPreview = (ImageView) view.findViewById(R.id.imgFullPhotoPreview);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        final String strFileItemName = fileItemsArrayList.get(position).getName();
        String strFileItemContainer = fileItemsArrayList.get(position).getContainer();

        /**Defining URL for fetching fileItemsArrayList*/
        final String token = SharedPreferenceManager.getSharedPreferenceManagerInstance()
                .getTokenFromShared();
        String tenantId = SharedPreferenceManager.getSharedPreferenceManagerInstance()
                .getTenantIdFromShared();

        final String url = Constants.API_BASE_URL_1 + "AUTH_" + tenantId + "/" +
                strFileItemContainer + "/" + strFileItemName;


        if (!CheckersUtils.getCheckersUtilsInstance().checkIfFolder(strFileItemName)) {
            if (CheckersUtils.getCheckersUtilsInstance().checkIfImageFile(strFileItemName)) {
                GlideUrl urls = new GlideUrl(url, new LazyHeaders.Builder()
                        .addHeader(Constants.API_AUTHENTICATION_TOKEN_TAG, token)
                        .build());

                Glide.with(mContext).load(urls)
                        .dontAnimate()
                        .override(Constants.GLIDE_IMAGE_SIZE_FULLSIZE_WIDTH,
                                Constants.GLIDE_IMAGE_SIZE_FULLSIZE_HEIGHT)
                        .placeholder(R.drawable.ic_container_unload_photo)
                        .diskCacheStrategy(DiskCacheStrategy.RESULT)
                        .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(imageViewPreview);

                Log.e(TAG, "instantiateItem: " + strFileItemName);
            } else if (CheckersUtils.getCheckersUtilsInstance().checkIfVideoFile(strFileItemName)) {
                Log.e(TAG, "instantiateItem: " + url);

            }
            container.addView(view);
        }
        return view;
    }

    @Override
    public int getCount() {
        return fileItemsArrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == (obj);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}
