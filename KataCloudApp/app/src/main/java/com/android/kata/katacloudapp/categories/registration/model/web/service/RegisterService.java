package com.android.kata.katacloudapp.categories.registration.model.web.service;

import android.content.Context;

public interface RegisterService {

    interface OnFinishedValidation {
        void onSuccess();

        void onError();
    }

    void doRegister(Context context, OnFinishedValidation listener, String email, String firsname, String lastname, String password);

}
