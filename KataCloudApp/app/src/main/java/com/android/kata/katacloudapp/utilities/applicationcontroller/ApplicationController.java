package com.android.kata.katacloudapp.utilities.applicationcontroller;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.database.greendao.db.DaoMaster;
import com.android.kata.katacloudapp.utilities.database.greendao.db.DaoSession;
import com.danikula.videocache.HttpProxyCacheServer;
import com.tsengvn.typekit.Typekit;

public class ApplicationController extends Application {

    DaoSession sDaoSession;
    private HttpProxyCacheServer proxy;

    @Override
    public void onCreate() {
        super.onCreate();
        setupGreendaoDb();
        setDefaultFontType();
    }

    public void setupGreendaoDb() {
        DaoMaster.DevOpenHelper masterHelper = new DaoMaster
                .DevOpenHelper(this, Constants.GREENDAO_DB_NAME, null); /**create database db file if not exist*/
        SQLiteDatabase db = masterHelper.getWritableDatabase();  /**Get the created database db file*/
        DaoMaster daoMaster = new DaoMaster(db);/**Create MasterDao*/
        sDaoSession = daoMaster.newSession();
    }

    public DaoSession getDaoSession() {
        return sDaoSession;
    }

    public void setDefaultFontType() {
        /**Set default font for the app */
        Typekit.getInstance()
                .addNormal(Typekit.createFromAsset(this, "montserrat_normal.otf"))
                .addBold(Typekit.createFromAsset(this, "montserrat_bold.otf"));
    }

    public static HttpProxyCacheServer getProxy(Context context) {
        ApplicationController app = (ApplicationController) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer(this);
    }
}
