package com.android.kata.katacloudapp.categories.login.model.web.api;

import com.android.kata.katacloudapp.categories.login.model.LoginModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface LoginAPIRoutes {

    @FormUrlEncoded
    @POST("KataCloud/login.php")
    Call<LoginModel> onLogin(
            @Header("X-Auth-Token") String token,
            @Field("username") String username,
            @Field("password") String password,
            @Field("system") String systemType,
            @Field("appversion") String appVersion);

}
