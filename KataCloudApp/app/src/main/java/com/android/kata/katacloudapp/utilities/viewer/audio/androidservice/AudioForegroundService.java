package com.android.kata.katacloudapp.utilities.viewer.audio.androidservice;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaCodec;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.widget.RemoteViews;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.utilities.checkers.CheckersUtils;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;
import com.android.kata.katacloudapp.utilities.viewer.CustomDebugTextView;
import com.android.kata.katacloudapp.utilities.viewer.video.exoplayerhelperclasses.DemoPlayer;
import com.android.kata.katacloudapp.utilities.viewer.video.exoplayerhelperclasses.ExtractorRendererBuilder;
import com.google.android.exoplayer.MediaCodecTrackRenderer;
import com.google.android.exoplayer.MediaCodecVideoTrackRenderer;

public class AudioForegroundService extends Service implements
        MediaCodecVideoTrackRenderer.EventListener,
        CustomDebugTextView.Provider.ProviderPlaybackInfo {


    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    /**
     * Variables
     */
    String token, strFileItemObjectName;
    boolean playerNeedsPrepare;
    Uri fileItemUri;

    /**
     * ExoPlayer Views
     */
    DemoPlayer player;
    Handler mHandler;
    boolean currentVersionSupportBigNotification = false;
    CustomDebugTextView customDebugTextView;

    /**
     * Notification views
     */
    RemoteViews smallNotificationView;


    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent data, int flags, int startId) {
        CheckersUtils.init();
        currentVersionSupportBigNotification = CheckersUtils.currentVersionSupportBigNotification();
        switch (data.getAction()) {
            case Constants.START_AUDIO_FOREGROUND_SERVICE:
                /**Getting string details for the view*/
                token = SharedPreferenceManager.getSharedPreferenceManagerInstance()
                        .getTokenFromShared();
                String tenantId = SharedPreferenceManager.getSharedPreferenceManagerInstance()
                        .getTenantIdFromShared();
                String strFileItemName = data.getStringExtra(Constants.INTENT_FILEPATH_KEY);
                strFileItemObjectName = data.getStringExtra(Constants.INTENT_FILEOBJECTNAME_KEY);
                String strFileItemContainer = data.getStringExtra(Constants.INTENT_CONTAINER_KEY);
                String url = Constants.API_BASE_URL_1 + "AUTH_" + tenantId + "/"
                        + strFileItemContainer + "/" + strFileItemName;
                fileItemUri = Uri.parse(url);

                /**Preparing player*/
                preparePlayer(true, token, fileItemUri);

                /**Show new notification*/
                showNotification(strFileItemObjectName, true);
                break;

            case Constants.STOP_AUDIO_FOREGROUND_SERVICE:
                releasePlayer();
                stopSelf();
                sendBroadcast(new Intent()
                        .putExtra(Constants.INTENT_AUDIO_NOTIFICATION_ACTION,
                                Constants.STOP_AUDIO_FOREGROUND_SERVICE)
                        .setAction(Constants.AUDIO_PLAYER_RECEIVER));
                break;

            case Constants.PLAY_AUDIO_FOREGROUND_SERVICE:
                if (player != null) {
                    showNotification(strFileItemObjectName, true);
                    player.getPlayerControl().start();
                } else {
                    preparePlayer(true, token, fileItemUri);
                }
                sendBroadcast(new Intent()
                        .putExtra(Constants.INTENT_AUDIO_NOTIFICATION_ACTION,
                                Constants.PLAY_AUDIO_FOREGROUND_SERVICE)
                        .setAction(Constants.AUDIO_PLAYER_RECEIVER));
                break;

            case Constants.PAUSE_AUDIO_FOREGROUND_SERVICE:
                if (player.getPlayerControl().canPause()) {
                    showNotification(strFileItemObjectName, false);
                    player.getPlayerControl().pause();

                    sendBroadcast(new Intent()
                            .putExtra(Constants.INTENT_AUDIO_NOTIFICATION_ACTION,
                                    Constants.PAUSE_AUDIO_FOREGROUND_SERVICE)
                            .setAction(Constants.AUDIO_PLAYER_RECEIVER));
                }
                break;

            case Constants.FAST_FORWARD_AUDIO_FOREGROUND_SERVICE:
                if (player.getPlayerControl().canSeekForward()) {
                    player.getPlayerControl().seekTo((int) player.getCurrentPosition() + 1500);
                }
                break;

            case Constants.REWIND_AUDIO_FOREGROUND_SERVICE:
                if (player.getPlayerControl().canSeekBackward()) {
                    player.getPlayerControl().seekTo((int) player.getCurrentPosition() - 5000);
                }
                break;
        }
        return super.onStartCommand(data, flags, startId);
    }

    private void preparePlayer(boolean playWhenReady, String token, Uri uri) {
        if (player == null) {
            player = new DemoPlayer(new ExtractorRendererBuilder(this, Constants.USER_AGENT, token, uri));
            playerNeedsPrepare = true;
            customDebugTextView = new CustomDebugTextView(this, player);
            customDebugTextView.start();
        }

        if (playerNeedsPrepare) {
            player.prepare();
            playerNeedsPrepare = false;
        }
        player.setPlayWhenReady(playWhenReady);
    }

    public void showNotification(String title, boolean status) {
        /**Setting up custom notification layout*/
        smallNotificationView = new RemoteViews(getApplication().getPackageName(),
                R.layout.service_music_notification);
        smallNotificationView.setTextViewText(R.id.txtFileTitle, title);

        isAudioPlaying(status);

        /**Setting up listeners for notification buttons*/
        setListeners(smallNotificationView);
        Notification notification = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.ic_container_music)
                .setContentTitle(title)
                .setContent(smallNotificationView)
                .setAutoCancel(false)
                .build();

        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        startForeground(Constants.NOTIFICATION_AUDIO_SERVICE, notification);

    }

    public void setListeners(RemoteViews view) {
        /**Setting up notification play button*/
        Intent playAudioIntent = new Intent(this, AudioForegroundService.class);
        playAudioIntent.setAction(Constants.PLAY_AUDIO_FOREGROUND_SERVICE);
        PendingIntent playAudioPendingIntent = PendingIntent.getService(this, 0,
                playAudioIntent, 0);
        view.setOnClickPendingIntent(R.id.btnPlay, playAudioPendingIntent);

        /**Setting up notification pause button*/
        Intent pauseAudioIntent = new Intent(this, AudioForegroundService.class);
        pauseAudioIntent.setAction(Constants.PAUSE_AUDIO_FOREGROUND_SERVICE);
        PendingIntent pauseAudioPendingIntent = PendingIntent.getService(this, 0,
                pauseAudioIntent, 0);
        view.setOnClickPendingIntent(R.id.btnPause, pauseAudioPendingIntent);

        /**Setting up notification close button*/
        Intent stopAudioIntent = new Intent(this, AudioForegroundService.class);
        stopAudioIntent.setAction(Constants.STOP_AUDIO_FOREGROUND_SERVICE);
        PendingIntent stopAudioPendingIntent = PendingIntent.getService(this, 0,
                stopAudioIntent, 0);
        view.setOnClickPendingIntent(R.id.btnClose, stopAudioPendingIntent);
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
            customDebugTextView.stop();
            customDebugTextView = null;
        }
    }

    private void isAudioPlaying(boolean status) {
        if (status) {
            smallNotificationView.setViewVisibility(R.id.btnPause, View.VISIBLE);
            smallNotificationView.setViewVisibility(R.id.btnPlay, View.GONE);
        } else {
            smallNotificationView.setViewVisibility(R.id.btnPause, View.GONE);
            smallNotificationView.setViewVisibility(R.id.btnPlay, View.VISIBLE);
        }
    }

    @Override
    public void onDroppedFrames(int count, long elapsed) {

    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {

    }

    @Override
    public void onDrawnToSurface(Surface surface) {

    }

    @Override
    public void onDecoderInitializationError(MediaCodecTrackRenderer.DecoderInitializationException e) {

    }

    @Override
    public void onCryptoError(MediaCodec.CryptoException e) {

    }

    @Override
    public void onDecoderInitialized(String decoderName, long elapsedRealtimeMs, long initializationDurationMs) {

    }

    @Override
    public void getPlaybackTime(long time) {
        Log.e(TAG, "getPlaybackTime: " + player.getPlayerControl().isPlaying());

        boolean playerStatus = player.getPlayerControl().isPlaying();
        sendBroadcast(new Intent()
                .putExtra(Constants.INTENT_AUDIO_NOTIFICATION_ACTION, Constants.GET_AUDIO_FOREGROUND_BUFFERED_TIME)
                .putExtra(Constants.INTENT_PLAYER_BUFFERED_TIME, time)
                .putExtra(Constants.INTENT_PLAYER_DURATION, player.getDuration())
                .putExtra(Constants.INTENT_PLAYER_BUFFERING_TIME, player.getPlayerControl().getBufferPercentage())
                .putExtra(Constants.INTENT_PLAYER_STATUS, playerStatus)
                .setAction(Constants.AUDIO_PLAYER_RECEIVER));
    }
}
