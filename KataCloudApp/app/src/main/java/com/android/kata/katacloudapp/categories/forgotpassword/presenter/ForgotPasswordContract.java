package com.android.kata.katacloudapp.categories.forgotpassword.presenter;

import android.content.Context;

public interface ForgotPasswordContract {

    interface ForgotPasswordPresenter {
        void onForgotPasswordListener(Context context, String email);
    }

    interface ForgotPasswordView {
        void onForgotPasswordCallback();
    }
}
