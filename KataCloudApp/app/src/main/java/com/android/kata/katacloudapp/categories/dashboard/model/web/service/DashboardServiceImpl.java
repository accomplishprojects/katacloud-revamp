package com.android.kata.katacloudapp.categories.dashboard.model.web.service;

import android.content.Context;
import android.util.Log;

import com.android.kata.katacloudapp.categories.dashboard.model.web.api.DashboardAPIProvider;
import com.android.kata.katacloudapp.utilities.checkers.CheckersUtils;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.database.greendao.db.FileItems;
import com.android.kata.katacloudapp.utilities.database.greendao.db.GreenDaoManager;
import com.android.kata.katacloudapp.utilities.network.RestManager;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardServiceImpl implements DashboardService {

    private String TAG = "Luigi";

    /**
     * Call for creating folder to server
     */
    @Override
    public void onCreateObjectToServer(final Context context, final String url, final String container, final String actionTag,
                                       final OnSuccessResponse onSuccessResponse, final OnErrorResponse onErrorResponse) {
        RestManager.init();
        SharedPreferenceManager.init(context);

        /**parameters*/
        String token = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTokenFromShared();
        final String tenantId = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTenantIdFromShared();

        Log.e(TAG, "onCreateObjectToServer: ACTION: " + actionTag + " URL: " + Constants.API_BASE_URL_1
                + "AUTH_" + tenantId + "/" + container + "/" + url);

        Call<ResponseBody> onCreateObjectCall = DashboardAPIProvider.getDashboardAPIRoutes()
                .onCreateObject(token, tenantId, container, url);
        onCreateObjectCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                /**Insert response into greendao db*/
                FileItems fileItems = new FileItems();
                fileItems.setTenant_id(tenantId);
                fileItems.setHash_code("");
                fileItems.setObject_name(CheckersUtils.getCheckersUtilsInstance()
                        .getFileItemObjectName(url));
                fileItems.setBytes("");
                fileItems.setName(url);
                fileItems.setContainer(container);
                fileItems.setDepth(CheckersUtils.getCheckersUtilsInstance().
                        getFileItemDepth(url));

                /**Insert FileItemObject into Greendao DB*/
                GreenDaoManager.getGreenDaoManagerInstance().getDaoSession().insert(fileItems);
                Log.e(TAG, "onResponse: " + url
                        + " ObjectName: " + CheckersUtils.getCheckersUtilsInstance().getFileItemObjectName(url)
                        + " Depths: " + CheckersUtils.getCheckersUtilsInstance().getFileItemDepth(url)
                        + " Container: " + container);

                onSuccessResponse.onSuccess(actionTag);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                onErrorResponse.onError();
            }
        });

    }

    /**
     * Call for deleting objects to server
     */

    @Override
    public void onDeleteObjectToServer(Context context, String url, String container, String actionTag,
                                       OnSuccessResponse onSuccessResponse, OnErrorResponse onErrorResponse) {
        RestManager.init();
        SharedPreferenceManager.init(context);

        /**parameters*/
        String token = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTokenFromShared();
        final String tenantId = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTenantIdFromShared();

        Log.e(TAG, "onCreateObjectToServer: ACTION: " + actionTag + " URL: " + Constants.API_BASE_URL_1
                + "AUTH_" + tenantId + "/" + container + "/" + url);

    }

}
