package com.android.kata.katacloudapp.categories.registration.model.web.api;

import com.android.kata.katacloudapp.utilities.network.RestManager;

import retrofit2.Retrofit;

public class RegisterAPIProvider {

    public static RegisterAPIRoutes getRegisterAPIRoutes() {
        Retrofit mRetrofit = RestManager.getRestManagerInstance().getRetrofitAdapter2();
        return mRetrofit.create(RegisterAPIRoutes.class);
    }
}
