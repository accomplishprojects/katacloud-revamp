package com.android.kata.katacloudapp.utilities.checkers;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;

public class CheckersUtils {

    private static CheckersUtils sCheckersUtils;

    public static void init() {
        if (sCheckersUtils == null) {
            sCheckersUtils = new CheckersUtils();
        }
    }

    public static CheckersUtils getCheckersUtilsInstance() {
        if (sCheckersUtils != null) {
            return sCheckersUtils;
        }
        throw new IllegalStateException("Call CheckerUtils init() method");
    }

    public int getFileItemDepth(String filePath) {
        /**Count '/' symbol as depth of the filePath*/
        int size;
        try {
            String[] dataFilter = filePath.split("/");
            size = dataFilter.length;
        } catch (Exception e) {
            return 0;
        }
        return size;
    }

    public String getFileItemObjectName(String filePath) {
        String strObjectName;
        if (!checkIfFolder(filePath)) {
            strObjectName = filePath.substring(filePath.lastIndexOf("/") + 1);
        } else {
            String[] words = filePath.split("/");
            strObjectName = words[words.length - 1];
        }
        return strObjectName;
    }

    public String removeLastDirectory(String inputString) {
        String reverseString = "";
        String newString = "";

        /**Reverse inputString*/
        for (int j = inputString.length() - 1; j >= 0; j--) {
            reverseString = reverseString + inputString.charAt(j);
        }

        /**Splitting inputString value by '/' sign*/
        String[] dataFilter = reverseString.split("/", 3);
        int size = dataFilter.length;

        /**Convert reverse string into normal string without the last directory*/
        reverseString = dataFilter[size - 1];
        for (int j = reverseString.length() - 1; j >= 0; j--) {
            newString = newString + reverseString.charAt(j);
        }
        return newString + "/";
    }

    public void setFileItemIcons(Context context, String url, String token, ImageView imageView) {

        /**Set thumbnail for Folders*/
        if (checkIfFolder(url)) {
            Glide.with(context).load(R.drawable.ic_container_folder).into(imageView);
        } else {
            /**Set thumbnail for photos*/
            if (CheckersUtils.getCheckersUtilsInstance().checkIfImageFile(url)) {
                GlideUrl urls = new GlideUrl(url, new LazyHeaders.Builder()
                        .addHeader(Constants.API_AUTHENTICATION_TOKEN_TAG, token)
                        .build());

                Glide.with(context).load(urls)
                        .override(Constants.GLIDE_IMAGE_SIZE_THUMBNAIL, Constants.GLIDE_IMAGE_SIZE_THUMBNAIL)
                        .centerCrop()
                        .dontAnimate()
                        .placeholder(R.drawable.ic_container_unload_photo)
                        .diskCacheStrategy(DiskCacheStrategy.RESULT)
                        .into(imageView);

                /**Set thumbnail for Misc*/
            } else if (url.contains(".txt")) {
                Glide.with(context).load(R.drawable.ic_container_document).into(imageView);

                /**Set thumbnail for Videos*/
            } else if (CheckersUtils.getCheckersUtilsInstance().checkIfVideoFile(url)) {
                Glide.with(context).load(R.drawable.ic_drawer_videos).into(imageView);

                /**Set thumbnail for Music*/
            } else if (url.contains(".mp3")) {
                Glide.with(context).load(R.drawable.ic_container_music).into(imageView);
            } else {
                Glide.with(context).load(R.drawable.ic_container_unknown_file).fitCenter().into(imageView);
            }

        }
    }


    public boolean checkIfFolder(String filepath) {
        return filepath.substring(filepath.length() - 1).equalsIgnoreCase("/") ? true : false;
    }

    public boolean checkIfVideoFile(String filepath) {
        if (filepath.contains(".mp4")) {
            return true;
        }
        return false;
    }

    public boolean checkIfImageFile(String filepath) {
        if (filepath.contains(".jpg") || filepath.contains(".png")) {
            return true;
        }
        return false;
    }

    public boolean checkIfAudioFile(String filepath) {
        if (filepath.contains(".mp3") || filepath.contains(".aac")) {
            return true;
        }
        return false;
    }

    public void checkIfContainerIsEmpty(int arraySize, TextView textView, String container) {
        if (arraySize > 0 && textView != null) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
        }
    }

    public String pathGenerator(Uri uri, Context context) {
        String filePath = null;
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (isKitKat) {
            filePath = pathGeneratorForKitkat(uri, context);
        }

        if (filePath != null) {
            return filePath;
        }

        Cursor cursor = context.getContentResolver().query(uri,
                new String[]{MediaStore.MediaColumns.DATA}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        return filePath == null ? uri.getPath() : filePath;
    }

    @TargetApi(19)
    private String pathGeneratorForKitkat(Uri uri, Context context) {
        String filePath = null;
        Cursor cursor = null;
        if (DocumentsContract.isDocumentUri(context, uri)) {

            String wholeID = DocumentsContract.getDocumentId(uri);
            String id = wholeID.split(":")[1];
            String[] column = {MediaStore.MediaColumns.DATA};

            String fileType = context.getContentResolver().getType(uri);
            if (fileType.startsWith("image")) {
                String sel = MediaStore.Images.Media._ID + "=?";
                cursor = context.getContentResolver().
                        query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                column, sel, new String[]{id}, null);
            } else if (fileType.startsWith("video")) {
                String sel = MediaStore.Video.Media._ID + "=?";
                cursor = context.getContentResolver().
                        query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                                column, sel, new String[]{id}, null);
            } else if (fileType.startsWith("audio")) {
                String sel = MediaStore.Audio.Media._ID + "=?";
                cursor = context.getContentResolver().
                        query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                                column, sel, new String[]{id}, null);
            }

            int columnIndex = cursor.getColumnIndex(column[0]);
            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }

            cursor.close();
        }
        return filePath;
    }

    public static boolean currentVersionSupportBigNotification() {
        int sdkVersion = android.os.Build.VERSION.SDK_INT;
        if (sdkVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            return true;
        }
        return false;
    }

    public static boolean checkIfServiceRunning(String serviceName, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceName.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
