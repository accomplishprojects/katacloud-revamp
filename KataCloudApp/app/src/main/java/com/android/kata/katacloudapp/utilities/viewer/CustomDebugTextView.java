package com.android.kata.katacloudapp.utilities.viewer;

import android.content.Context;
import android.os.Handler;
import android.widget.TextView;

import com.android.kata.katacloudapp.utilities.viewer.audio.views.AudioViewerActivity;
import com.android.kata.katacloudapp.utilities.viewer.video.exoplayerhelperclasses.DemoPlayer;
import com.google.android.exoplayer.CodecCounters;
import com.google.android.exoplayer.chunk.Format;
import com.google.android.exoplayer.upstream.BandwidthMeter;

/**
 * A helper class for periodically updating debug information displayed by a {@link TextView}.
 */
public class CustomDebugTextView implements Runnable {


    /**
     * Provides debug information about an ongoing playback.
     */
    public interface Provider {

        interface ProviderPlaybackInfo {
            void getPlaybackTime(long time);
        }

        /**
         * Returns the current playback position, in milliseconds.
         */
        long getCurrentPosition();

        /**
         * Returns a format whose information should be displayed, or null.
         */
        Format getFormat();

        /**
         * Returns a {@link BandwidthMeter} whose estimate should be displayed, or null.
         */
        BandwidthMeter getBandwidthMeter();

        /**
         * Returns a {@link CodecCounters} whose information should be displayed, or null.
         */
        CodecCounters getCodecCounters();


    }

    private static final int REFRESH_INTERVAL_MS = 1000;
    //    private final TextView textView;
    Handler mHandler;
    private final com.google.android.exoplayer.util.DebugTextViewHelper.Provider debuggable;
    private Provider.ProviderPlaybackInfo providerPlaybackInfo;

    /**
     * @param debuggable The {@link com.google.android.exoplayer.util.DebugTextViewHelper.Provider} from which debug information should be obtained.
     */
    public CustomDebugTextView(Context context, com.google.android.exoplayer.util.DebugTextViewHelper.Provider debuggable) {
        providerPlaybackInfo = (Provider.ProviderPlaybackInfo) context;
        this.debuggable = debuggable;
        mHandler = new Handler();
    }

    /**
     * Starts periodic updates of the {@link TextView}.
     * <p>
     * Should be called from the application's main thread.
     */
    public void start() {
        stop();
        run();
    }

    /**
     * Stops periodic updates of the {@link TextView}.
     * <p>
     * Should be called from the application's main thread.
     */
    public void stop() {
//        textView.removeCallbacks(this);
        mHandler.removeCallbacks(this);
    }

    @Override
    public void run() {
//        textView.setText(getRenderString());
//        textView.postDelayed(this, REFRESH_INTERVAL_MS);
        getRenderString();
        mHandler.postDelayed(this, REFRESH_INTERVAL_MS);
    }

    private String getRenderString() {
        providerPlaybackInfo.getPlaybackTime(debuggable.getCurrentPosition());
        return getTimeString() + " " + getQualityString() + " " + getBandwidthString() + " "
                + getVideoCodecCountersString();

    }

    private String getTimeString() {
        return "ms(" + debuggable.getCurrentPosition() + ")";
    }

    private String getQualityString() {
        Format format = debuggable.getFormat();
        return format == null ? "id:? br:? h:?"
                : "id:" + format.id + " br:" + format.bitrate + " h:" + format.height;
    }

    private String getBandwidthString() {
        BandwidthMeter bandwidthMeter = debuggable.getBandwidthMeter();
        if (bandwidthMeter == null
                || bandwidthMeter.getBitrateEstimate() == BandwidthMeter.NO_ESTIMATE) {
            return "bw:?";
        } else {
            return "bw:" + (bandwidthMeter.getBitrateEstimate() / 1000);
        }
    }

    private String getVideoCodecCountersString() {
        CodecCounters codecCounters = debuggable.getCodecCounters();
        return codecCounters == null ? "" : codecCounters.getDebugString();
    }

}
