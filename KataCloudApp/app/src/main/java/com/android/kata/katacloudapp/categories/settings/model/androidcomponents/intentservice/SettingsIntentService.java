package com.android.kata.katacloudapp.categories.settings.model.androidcomponents.intentservice;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.android.kata.katacloudapp.categories.settings.model.web.api.SettingsAPIProvider;
import com.android.kata.katacloudapp.utilities.network.RestManager;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class SettingsIntentService extends IntentService {

    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    public SettingsIntentService() {
        super("SettingsIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        RestManager.init();
        SharedPreferenceManager.init(getApplicationContext());

        String token = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTokenFromShared();
        String tenantId = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTenantIdFromShared();

        Call<ResponseBody> onGetSettingsCall = SettingsAPIProvider.getSettingsAPIRoutes()
                .onGetSettings(token, tenantId);
        onGetSettingsCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (response.raw().isSuccessful()) {
                    String strStorageUsed = response.headers().get("X-Account-Bytes-Used");
                    String strAllocatedStorage = response.headers().get("X-Account-Meta-Quota-Bytes");
                    SharedPreferenceManager.getSharedPreferenceManagerInstance()
                            .putSettingsDetailsToShared(strStorageUsed, strAllocatedStorage);
                } else {
                    Log.e(TAG, "onResponse: " + "Something went wrong in response");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onResponse: " + t.getMessage());
            }
        });

        stopSelf();

    }
}
