package com.android.kata.katacloudapp.categories.forgotpassword.model.web.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ForgotPasswordAPIRoutes {

    @FormUrlEncoded
    @POST("KataCloud/SendResetPassword.php")
    Call<ResponseBody> onForgotPassword(@Field("email") String email);

}
