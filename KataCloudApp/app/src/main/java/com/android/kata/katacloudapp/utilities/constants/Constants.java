package com.android.kata.katacloudapp.utilities.constants;

public class Constants {

    /**
     * API URLS
     */
    public final static String API_BASE_URL_1 = "https://proxy.kata-cloud.com:8443/v1/";
    public final static String API_BASE_URL_2 = "https://bridge.kata-cloud.com/";
    public final static String API_BASE_URL_3 = "https://storageupgrade.kata-cloud.com/";

    /**
     * Upgrade account values
     */
    public final static String API_UPGRADE_ACCOUNT_REQUEST_CODE_VALUE = "plumFlavoredIcecandy";


    /**
     * API Tags
     */
    public final static String API_AUTHENTICATION_TOKEN_TAG = "X-Auth-Token";
    public final static String API_CONTENT_TYPE_TAG = "Content-Type";

    /**
     * SharedPreferences Tags
     */
    public final static String SHAREDPREFERENCES_FILENAME = "KataCloudPreferences";
    public final static String SHAREDPREFERENCES_TOKEN_KEY = "userToken";
    public final static String SHAREDPREFERENCES_TENANT_ID_KEY = "userTenantId";
    public final static String SHAREDPREFERENCES_USER_TENANT_NAME_KEY = "userTenantName";
    public final static String SHAREDPREFERENCES_USER_FIRSTNAME_KEY = "userFirstname";
    public final static String SHAREDPREFERENCES_USER_LASTNAME_KEY = "userLastname";
    public final static String SHAREDPREFERENCES_STORAGE_USED = "userStorageUsed";
    public final static String SHAREDPREFERENCES_ALLOCATED_STORAGE = "userAllocatedStorage";

    /**
     * Container Tags
     */
    public final static String CONTAINER_PHOTOS_TAG = "photos";
    public final static String CONTAINER_VIDEOS_TAG = "videos";
    public final static String CONTAINER_MUSIC_TAG = "music";
    public final static String CONTAINER_MISC_TAG = "misc";

    /**
     * Receiver Tags
     */
    public final static String SYNCHRONIZATION_RECEIVER = "com.android.kata.katacloudapp.DashboardReceiver";
    public final static String AUDIO_PLAYER_RECEIVER = "com.android.kata.katacloudapp.utilities.viewer.audio.views.receiver";

    /**
     * GreenDao Tags
     */
    public final static String GREENDAO_DB_NAME = "kata_cloud_db";

    /**
     * GridView Tags
     */
    public final static int GRIDVIEW_COLUMN_NUMBER = 3;

    /**
     * Action Tags
     */
    public final static String ACTION_SYNCHRONIZE_TAG = "Synchronize";
    public final static String ACTION_CREATE_TAG = "Create";
    public final static String ACTION_DELETE_TAG = "Delete";
    public final static String ACTION_RENAME_TAG = "Rename";
    public final static String ACTION_DOWNLOAD_TAG = "Download";
    public final static String ACTION_UPLOAD_TAG = "Upload";

    /**
     * Intent Tags
     */
    public final static String INTENT_CONTAINER_KEY = "container";
    public final static String INTENT_ACTIONTAG_KEY = "actionTag";
    public final static String INTENT_FILEPATH_KEY = "filepath";
    public final static String INTENT_FILEOBJECTNAME_KEY = "fileObjectName";
    public final static String INTENT_ACTUAL_FILEPATH_KEY = "actualFilePath";
    public final static String INTENT_CONTENT_TYPE_EXTRA = "content_type";
    public final static String INTENT_CONTENT_EXT_EXTRA = "type";
    public final static String INTENT_AUDIO_NOTIFICATION_ACTION = "audioForegroundAction";
    public final static String INTENT_PLAYER_BUFFERED_TIME = "playerCurrentPosition";
    public final static String INTENT_PLAYER_BUFFERING_TIME = "playerBufferingPosition";
    public final static String INTENT_PLAYER_DURATION = "playerDuration";
    public final static String INTENT_PLAYER_STATUS = "playerStatus";


    /**
     * External Storage Tags
     */
    public final static String EXTERNAL_STORAGE_DOWNLOAD_PATH = "/sdcard/Kata/Downloads/";

    /**
     * REQUEST CODE Tags
     */
    public final static int FILE_CHOOSER_REQUESTCODE = 1000;
    public final static int PERMISSION_REQUESTCODE = 1001;

    /**
     * Notification Tags
     */
    public final static int NOTIFICATION_DOWNLOAD_TAG = 2000;
    public final static int NOTIFICATION_UPLOAD_TAG = 2001;
    public final static int NOTIFICATION_AUDIO_SERVICE = 2002;

    /**
     * Glide Tags
     */
    public final static int GLIDE_IMAGE_SIZE_THUMBNAIL = 80;
    public final static int GLIDE_IMAGE_SIZE_FULLSIZE_WIDTH = 300;
    public final static int GLIDE_IMAGE_SIZE_FULLSIZE_HEIGHT = 200;

    /**
     * Bundle Tags
     */
    public final static String BUNDLE_PHOTO_VIEWER_KEY = "photoViewerBundle";
    public final static String BUNDLE_FILE_ITEM_OBJECT_KEY = "fileItemObject";
    public final static String BUNDLE_FILE_ITEM_POSITION_KEY = "fileItemPosition";

    /**
     * HashMap Tags
     */
    public final static String HASHMAP_IMAGE_TAG = "image";
    public final static String HASHMAP_ORIGINAL_POSITION_TAG = "originalPosition";
    public final static String HASHMAP_FILTERED_POSITION_TAG = "filteredPosition";

    /**
     * ExoPlayer Tags
     */
    public final static String USER_AGENT = "KataCloudApp";
    public final static int BUFFER_SEGMENT_SIZE = 64 * 1024;
    public final static int BUFFER_SEGMENT_COUNT = 256;

    /**
     * Android Service
     */
    public final static String START_AUDIO_FOREGROUND_SERVICE = "com.android.kata.katacloudapp.utilities.viewer.audio.androidservice.startservice";
    public final static String STOP_AUDIO_FOREGROUND_SERVICE = "com.android.kata.katacloudapp.utilities.viewer.audio.androidservice.stopservice";
    public final static String PLAY_AUDIO_FOREGROUND_SERVICE = "com.android.kata.katacloudapp.utilities.viewer.audio.androidservice.playaudio";
    public final static String PAUSE_AUDIO_FOREGROUND_SERVICE = "com.android.kata.katacloudapp.utilities.viewer.audio.androidservice.pauseaudio";
    public final static String GET_AUDIO_FOREGROUND_BUFFERED_TIME = "com.android.kata.katacloudapp.utilities.viewer.audio.androidservice.fetchbufferedposition";
    public final static String FAST_FORWARD_AUDIO_FOREGROUND_SERVICE = "com.android.kata.katacloudapp.utilities.viewer.audio.androidservice.fastforward";
    public final static String REWIND_AUDIO_FOREGROUND_SERVICE = "com.android.kata.katacloudapp.utilities.viewer.audio.androidservice.rewind";

}
