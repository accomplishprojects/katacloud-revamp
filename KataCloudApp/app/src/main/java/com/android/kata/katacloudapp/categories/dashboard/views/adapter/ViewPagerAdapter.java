package com.android.kata.katacloudapp.categories.dashboard.views.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.android.kata.katacloudapp.categories.dashboard.views.photos.PhotosFragment;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
//        Fragment fragment = null;

//        switch (position) {
//            case 0:
//                fragment = mFragmentList.get(0);
//                break;
//            case 1:
//                fragment = mFragmentList.get(1);
//                break;
//            case 2:
//                fragment = mFragmentList.get(2);
//                break;
//            case 3:
//                fragment = mFragmentList.get(3);
//                break;
//        }

        return mFragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentTitleList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
