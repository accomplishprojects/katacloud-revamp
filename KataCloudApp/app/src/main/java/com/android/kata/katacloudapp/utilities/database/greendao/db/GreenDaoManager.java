package com.android.kata.katacloudapp.utilities.database.greendao.db;

import android.content.Context;

import com.android.kata.katacloudapp.utilities.applicationcontroller.ApplicationController;

import java.util.List;

public class GreenDaoManager {

    private static GreenDaoManager sGreenDaoManager;
    private static DaoSession sDaoSession;

    public static void init(Context context) {
        if (sGreenDaoManager == null) {
            sGreenDaoManager = new GreenDaoManager();
            sDaoSession = ((ApplicationController) context.getApplicationContext()).getDaoSession();
        }
    }

    public static GreenDaoManager getGreenDaoManagerInstance() {
        if (sGreenDaoManager != null) {
            return sGreenDaoManager;
        }

        throw new IllegalStateException("Call GreenDaoManager init()");
    }

    public DaoSession getDaoSession() {
        return sDaoSession;
    }

    public List<FileItems> getAllFileItems() {
        return sDaoSession.getFileItemsDao().loadAll();
    }

    public List<FileItems> getMainFileItemDirectory(int depth, String container) {
        return sDaoSession.getFileItemsDao().queryBuilder().where(
                FileItemsDao.Properties.Depth.eq(depth),
                FileItemsDao.Properties.Container.eq(container))
                .orderAsc(FileItemsDao.Properties.Object_name)
                .list();
    }

    public List<FileItems> getDirectoryFiles(String filePath, int depth, String container) {
        return sDaoSession.getFileItemsDao().queryBuilder().where(
                FileItemsDao.Properties.Name.like(filePath + "%"),
                FileItemsDao.Properties.Depth.eq(depth),
                FileItemsDao.Properties.Container.eq(container))
                .orderAsc(FileItemsDao.Properties.Object_name)
                .list();
    }

    public List<FileItems> getAllPhotos(String filepath, int depth, String container) {
        return sDaoSession.getFileItemsDao().queryBuilder().where(
                FileItemsDao.Properties.Name.like("%" + ".jpg"),
                FileItemsDao.Properties.Depth.eq(depth),
                FileItemsDao.Properties.Container.eq(container))
                .orderAsc(FileItemsDao.Properties.Object_name)
                .list();
    }

    public void deleteAllFileItems() {
        GreenDaoManager.getGreenDaoManagerInstance().getDaoSession()
                .getFileItemsDao().deleteAll();
    }

}
