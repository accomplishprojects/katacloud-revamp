package com.android.kata.katacloudapp.categories.forgotpassword.model.web.service;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.kata.katacloudapp.categories.forgotpassword.model.web.api.ForgotPasswordAPIProvider;
import com.android.kata.katacloudapp.utilities.network.RestManager;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordServiceImpl implements ForgotPasswordService {

    /**
     * Flags
     */
    private String TAG = this.getClass().getSimpleName();

    @Override
    public void doForgotPassword(final Context context, String email, OnSuccessResponse onSuccessResponse,
                                 OnErrorResponse onErrorResponse) {
        RestManager.init();

        Call<ResponseBody> onForgotPasswordCall = ForgotPasswordAPIProvider.getForgotPasswordAPIRoutes()
                .onForgotPassword(email);
        onForgotPasswordCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String strResponse = response.body().string();
                    Log.e(TAG, "onResponse: " + strResponse);
                    if (strResponse.equalsIgnoreCase("1")) {
                        Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
}
