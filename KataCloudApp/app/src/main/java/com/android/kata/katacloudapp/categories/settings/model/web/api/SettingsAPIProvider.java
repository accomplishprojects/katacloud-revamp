package com.android.kata.katacloudapp.categories.settings.model.web.api;

import com.android.kata.katacloudapp.utilities.network.RestManager;

import retrofit2.Retrofit;

public class SettingsAPIProvider {

    public static SettingsAPIRoutes getSettingsAPIRoutes() {
        Retrofit mRetrofit = RestManager.getRestManagerInstance().getRetrofitAdapter1();
        return mRetrofit.create(SettingsAPIRoutes.class);
    }
}
