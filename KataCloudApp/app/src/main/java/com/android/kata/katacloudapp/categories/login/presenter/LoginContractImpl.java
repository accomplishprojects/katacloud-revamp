package com.android.kata.katacloudapp.categories.login.presenter;

import android.content.Context;

import com.android.kata.katacloudapp.categories.login.model.web.service.LoginService;
import com.android.kata.katacloudapp.categories.login.model.web.service.LoginServiceImpl;

public class LoginContractImpl implements LoginContract.LoginPresenter,
        LoginService.OnFinishedValidation {

    LoginContract.LoginView loginView;
    LoginService loginService;

    public LoginContractImpl(LoginContract.LoginView view) {
        loginView = view;
        loginService = new LoginServiceImpl();
    }

    @Override
    public void loginListener(Context context, String username, String password) {
        loginService.doLogin(context, this, username, password);
    }

    @Override
    public void onSuccess() {
        if (loginView != null) {
            loginView.loginSuccessCallback();
        }
    }

    @Override
    public void onError() {
        if (loginView != null) {
            loginView.loginErrorCallback();
        }
    }
}
