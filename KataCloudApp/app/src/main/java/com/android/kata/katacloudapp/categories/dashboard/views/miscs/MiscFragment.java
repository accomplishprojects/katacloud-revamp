package com.android.kata.katacloudapp.categories.dashboard.views.miscs;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.dashboard.presenter.DashboardContract;
import com.android.kata.katacloudapp.categories.dashboard.views.adapter.ContainerAdapter;
import com.android.kata.katacloudapp.utilities.checkers.CheckersUtils;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.database.greendao.db.FileItems;
import com.android.kata.katacloudapp.utilities.database.greendao.db.GreenDaoManager;
import com.android.kata.katacloudapp.utilities.viewer.audio.views.AudioViewerActivity;
import com.android.kata.katacloudapp.utilities.viewer.photo.helperclasses.PhotoViewerHelper;
import com.android.kata.katacloudapp.utilities.viewer.photo.views.PhotoViewerActivity;
import com.android.kata.katacloudapp.utilities.viewer.video.exoplayerhelperclasses.ExoPlayerActivity;
import com.google.android.exoplayer.util.Util;

import java.util.ArrayList;
import java.util.List;

public class MiscFragment extends Fragment {

    String TAG = this.getClass().getSimpleName();
    private View rootView;
    TextView txtDirectoryPath;
    private RecyclerView fileItemsRecyclerView;
    private ContainerAdapter containerAdapter;
    private List<FileItems> fileItemsList;
    String CONTAINER_TAG = Constants.CONTAINER_MISC_TAG;
    int containerDepth = 1;
    private DashboardContract.DashboardView dashboardView;
    private TextView txtEmptyContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_container, container, false);
        GreenDaoManager.init(getActivity());
        CheckersUtils.init();
        PhotoViewerHelper.init();
        setupViews();

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dashboardView = (DashboardContract.DashboardView) getActivity();
    }

    private void setupViews() {
        txtDirectoryPath = (TextView) rootView.findViewById(R.id.txtDirectoryPath);
        txtEmptyContainer = (TextView) rootView.findViewById(R.id.txtEmptyContainer);
        fileItemsList = new ArrayList<>();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fileItemsList.addAll(GreenDaoManager.getGreenDaoManagerInstance()
                        .getMainFileItemDirectory(containerDepth, CONTAINER_TAG));
                containerAdapter = new ContainerAdapter(getActivity(), fileItemsList, CONTAINER_TAG);
                fileItemsRecyclerView = (RecyclerView) rootView.findViewById(R.id.fileItemsRecyclerView);
                fileItemsRecyclerView.setHasFixedSize(true);
                fileItemsRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), Constants.GRIDVIEW_COLUMN_NUMBER));
                fileItemsRecyclerView.setAdapter(containerAdapter);
            }
        }, 0);
    }

    public void loadAllMiscFiles() {
        txtDirectoryPath.setText("");
        if (fileItemsList != null) {
            fileItemsList.clear();
            fileItemsList.addAll(GreenDaoManager.getGreenDaoManagerInstance()
                    .getMainFileItemDirectory(containerDepth, CONTAINER_TAG));
            containerAdapter.notifyDataSetChanged();
        }

        /**Show image for empty container*/
        CheckersUtils.getCheckersUtilsInstance()
                .checkIfContainerIsEmpty(fileItemsList.size(), txtEmptyContainer, CONTAINER_TAG);
    }

    public void onFolderSelected(String filepath, int depth) {
        Log.e(TAG, "onFolderSelected: " + filepath);
        Log.e(TAG, "onFolderSelected: " + depth);
        containerDepth++;
        Log.e(TAG, "onFolderSelected: " + containerDepth);
        txtDirectoryPath.setText(filepath);

        if (fileItemsList != null) {
            fileItemsList.clear();
            fileItemsList.addAll(GreenDaoManager.getGreenDaoManagerInstance()
                    .getDirectoryFiles(filepath, containerDepth, CONTAINER_TAG));
            containerAdapter.notifyDataSetChanged();
        }

        /**Show image for empty container*/
        CheckersUtils.getCheckersUtilsInstance()
                .checkIfContainerIsEmpty(fileItemsList.size(), txtEmptyContainer, CONTAINER_TAG);
    }

    public void onFileSelected(int position) {
        String fileItemName = fileItemsList.get(position).getName();
        String fileItemObjectName = fileItemsList.get(position).getObject_name();

        Log.e(TAG, "onFileSelected: " + fileItemName);
        if (CheckersUtils.getCheckersUtilsInstance().checkIfImageFile(fileItemName)) {
            startActivity(new Intent(getActivity(), PhotoViewerActivity.class)
                    .putExtra(Constants.BUNDLE_PHOTO_VIEWER_KEY,
                            PhotoViewerHelper.getCustomPhotoViewerInstance()
                                    .getViewableFilesBundle(fileItemsList, position)));
        } else if (CheckersUtils.getCheckersUtilsInstance().checkIfVideoFile(fileItemName)) {
            startActivity(new Intent(getActivity(), ExoPlayerActivity.class)
                    .putExtra(Constants.INTENT_FILEPATH_KEY, fileItemName)
                    .putExtra(Constants.INTENT_FILEOBJECTNAME_KEY, fileItemObjectName)
                    .putExtra(Constants.INTENT_CONTAINER_KEY, CONTAINER_TAG)
                    .putExtra(Constants.INTENT_CONTENT_TYPE_EXTRA, Util.TYPE_OTHER));
        } else if (CheckersUtils.getCheckersUtilsInstance().checkIfAudioFile(fileItemName)) {
            startActivity(new Intent(getActivity(), AudioViewerActivity.class)
                    .putExtra(Constants.INTENT_FILEPATH_KEY, fileItemName)
                    .putExtra(Constants.INTENT_FILEOBJECTNAME_KEY, fileItemObjectName)
                    .putExtra(Constants.INTENT_CONTAINER_KEY, CONTAINER_TAG)
                    .putExtra(Constants.INTENT_CONTENT_TYPE_EXTRA, Util.TYPE_OTHER));
        }
    }


    public void onBackPressed() {
        Log.e(TAG, "onBackPressed: " + containerDepth);
        String filepath = txtDirectoryPath.getText().toString();
        fileItemsList.clear();
        if (containerDepth > 1) {
            containerDepth--;
            if (containerDepth == 1) {
                txtDirectoryPath.setText("");
                fileItemsList.addAll(GreenDaoManager.getGreenDaoManagerInstance()
                        .getMainFileItemDirectory(containerDepth, CONTAINER_TAG));
            } else {
                txtDirectoryPath.setText(CheckersUtils.getCheckersUtilsInstance()
                        .removeLastDirectory(filepath));
                fileItemsList.addAll(GreenDaoManager.getGreenDaoManagerInstance()
                        .getDirectoryFiles(txtDirectoryPath.getText().toString(), containerDepth, CONTAINER_TAG));
            }
        } else {
            txtDirectoryPath.setText("");
            fileItemsList.addAll(GreenDaoManager.getGreenDaoManagerInstance()
                    .getMainFileItemDirectory(containerDepth, CONTAINER_TAG));
        }
        containerAdapter.notifyDataSetChanged();

        /**Show image for empty container*/
        CheckersUtils.getCheckersUtilsInstance()
                .checkIfContainerIsEmpty(fileItemsList.size(), txtEmptyContainer, CONTAINER_TAG);
    }

    public void onFloatingButtonPressed() {
        String filepath = txtDirectoryPath.getText().toString();
        dashboardView.onBottomSheetListener(filepath, R.menu.bottom_sheet_menu_fab, CONTAINER_TAG);
    }

    public void refreshContainerLayout() {
        Log.e(TAG, "refreshContainerLayout: " + containerDepth);
        fileItemsList.clear();
        if (containerDepth == 1) {
            fileItemsList.addAll(GreenDaoManager.getGreenDaoManagerInstance()
                    .getMainFileItemDirectory(containerDepth, CONTAINER_TAG));
        } else if (containerDepth > 1) {
            fileItemsList.addAll(GreenDaoManager.getGreenDaoManagerInstance()
                    .getDirectoryFiles(txtDirectoryPath.getText().toString(), containerDepth, CONTAINER_TAG));
        }
        containerAdapter.notifyDataSetChanged();

        /**Show image for empty container*/
        CheckersUtils.getCheckersUtilsInstance()
                .checkIfContainerIsEmpty(fileItemsList.size(), txtEmptyContainer, CONTAINER_TAG);
    }

    public void onUploadButtonPressed(Intent data) {
        String filepath = txtDirectoryPath.getText().toString();
        dashboardView.onUploadButtonPressedListener(data, filepath, CONTAINER_TAG);
    }
}
