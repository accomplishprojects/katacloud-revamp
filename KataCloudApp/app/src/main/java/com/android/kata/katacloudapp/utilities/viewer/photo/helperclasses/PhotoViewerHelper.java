package com.android.kata.katacloudapp.utilities.viewer.photo.helperclasses;

import android.os.Bundle;
import android.util.Log;

import com.android.kata.katacloudapp.utilities.checkers.CheckersUtils;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.database.greendao.db.FileItems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PhotoViewerHelper {

    /**
     * Flags
     */
    private String TAG = this.getClass().getSimpleName();

    private static PhotoViewerHelper sPhotoViewerHelper;

    public static void init() {
        if (sPhotoViewerHelper == null) {
            sPhotoViewerHelper = new PhotoViewerHelper();
        }
    }

    public static PhotoViewerHelper getCustomPhotoViewerInstance() {
        if (sPhotoViewerHelper != null) {
            return sPhotoViewerHelper;
        }
        throw new IllegalStateException("Call CustomPhotoViewer init()");
    }

    public Bundle getViewableFilesBundle(List<FileItems> fileItemList, int position) {

        /**Initializing arrays*/
        List<FileItems> fileItemsListOriginal = new ArrayList<>();
        ArrayList<HashMap<String, String>> fileItemListDuplicator = new ArrayList<>();
        ArrayList<FileItems> fileItemListFiltered = new ArrayList<>();
        HashMap<String, String> hashMap;

        fileItemsListOriginal.addAll(fileItemList);

        /**Initialize array counters*/
        int fileItemsCounter = 0;
        int filteredFileItemsCounter = 0;

        /**Loop original fileItemsListOriginal*/
        for (FileItems fileItems : fileItemsListOriginal) {
            hashMap = new HashMap<>();
            hashMap.put(Constants.HASHMAP_IMAGE_TAG, fileItems.getName());
            hashMap.put(Constants.HASHMAP_ORIGINAL_POSITION_TAG, String.valueOf(fileItemsCounter++));

            /**Filtering image files only*/
            if (CheckersUtils.getCheckersUtilsInstance().checkIfImageFile(fileItems.getName())) {
                hashMap.put(Constants.HASHMAP_FILTERED_POSITION_TAG, String.valueOf(filteredFileItemsCounter++));
                fileItemListFiltered.add(fileItems);
            }

            fileItemListDuplicator.add(hashMap);
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_FILE_ITEM_OBJECT_KEY, fileItemListFiltered);
        for (HashMap map : fileItemListDuplicator) {
            if (position == Integer.parseInt(map.get(Constants.HASHMAP_ORIGINAL_POSITION_TAG).toString())) {
                Log.e(TAG, "onFileSelected: FilteredPosition: " + map.get(Constants.HASHMAP_FILTERED_POSITION_TAG));
                bundle.putInt(Constants.BUNDLE_FILE_ITEM_POSITION_KEY,
                        Integer.parseInt(map.get(Constants.HASHMAP_FILTERED_POSITION_TAG).toString()));
            }
        }
        return bundle;
    }


}
