package com.android.kata.katacloudapp.categories.login.model.web.service;

import android.content.Context;

public interface LoginService {

    interface OnFinishedValidation {
        void onSuccess();
        void onError();
    }

    void doLogin(Context context, OnFinishedValidation listener, String username, String password);

}
