package com.android.kata.katacloudapp.categories.dashboard.presenter;

import android.content.Context;
import android.content.Intent;

import com.android.kata.katacloudapp.utilities.database.greendao.db.FileItems;

import java.util.ArrayList;
import java.util.List;

public interface DashboardContract {

    interface DashboardPresenter {
        void onCreateListener(Context context, String filepath, String container, String actionTag);

        void onDeleteListener(Context context, String filepath, String container, String actionTag);
    }

    interface DashboardView {
        void onFolderSelectedListener(String filePath, int depth, String container);

        void onFileSelectedListener(String container, int position);

        void onContainerBackPressedListener();

        void onBottomSheetListener(String title, int layout, String container);

        void onUploadButtonPressedListener(Intent data, String filepath, String container);

        void onSuccessCallback(String actionTag);

        void onErrorCallback();
    }
}
