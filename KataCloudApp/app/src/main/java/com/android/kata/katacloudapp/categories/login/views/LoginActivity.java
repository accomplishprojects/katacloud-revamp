package com.android.kata.katacloudapp.categories.login.views;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.baseactivity.BaseActivity;
import com.android.kata.katacloudapp.categories.dashboard.views.DashboardActivity;
import com.android.kata.katacloudapp.categories.forgotpassword.views.ForgotPasswordActivity;
import com.android.kata.katacloudapp.categories.login.presenter.LoginContract;
import com.android.kata.katacloudapp.categories.login.presenter.LoginContractImpl;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.device.DeviceManager;
import com.android.kata.katacloudapp.utilities.permissions.PermissionManager;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;

public class LoginActivity extends BaseActivity implements View.OnClickListener,
        LoginContract.LoginView {

    String strUsername, strPassword;
    EditText edtUsername, edtPassword;
    Button btnLogin;
    TextView txtForgotPassword;
    LoginContract.LoginPresenter loginPresenter;

    String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setupViews();

        DeviceManager.init(this);
        SharedPreferenceManager.init(this);
        PermissionManager.init();

        if (SharedPreferenceManager.getSharedPreferenceManagerInstance().getTokenFromShared() != null)
            startActivity(new Intent(this, DashboardActivity.class));

        loginPresenter = new LoginContractImpl(this);

    }

    private void setupViews() {
        edtUsername = (EditText) findViewById(R.id.edtUsername);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        txtForgotPassword = (TextView) findViewById(R.id.txtForgotPassword);
        txtForgotPassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                if (PermissionManager.getPermissionManagerInstance().isExternalStorageAllowed(this)) {
                    strUsername = edtUsername.getText().toString();
                    strPassword = edtPassword.getText().toString();
                    loginPresenter.loginListener(this, strUsername, strPassword);
                    showProgressDialog();
                } else {
                    PermissionManager.getPermissionManagerInstance().askPermission(this);
                }
                break;

            case R.id.txtForgotPassword:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                break;
        }
    }

    @Override
    public void loginSuccessCallback() {
        hideProgressDialog();
        startActivity(new Intent(this, DashboardActivity.class));
        finish();
    }

    @Override
    public void loginErrorCallback() {
        hideProgressDialog();
        Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == Constants.PERMISSION_REQUESTCODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "onRequestPermissionsResult: " + " Permission Granted");
            } else {
                Log.e(TAG, "onRequestPermissionsResult: " + "Permission Denied for model: " +
                        DeviceManager.getDeviceManagerInstance().getBrand() + "-"
                        + DeviceManager.getDeviceManagerInstance().getModel());
            }
        }
    }
}
