package com.android.kata.katacloudapp.categories.settings.views.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.upgradeaccount.views.UpgradeAccountActivity;
import com.android.kata.katacloudapp.utilities.converters.DataConverters;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;

public class SettingsFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener {

    EditTextPreference edtPrefEmail, edtPrefSpaceUsed;
    Preference prefUpdgradeAccount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_preferences);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferenceManager.init(getActivity());
        setupViews();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void setupViews() {
        String tenantName = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTenantNameFromShared();
        String storageUsed = DataConverters.convertBytesToReadableSize(Long.parseLong(
                SharedPreferenceManager.getSharedPreferenceManagerInstance().getUserStorageUsed()), true);
        String allocatedStorage = DataConverters.convertBytesToReadableSize(Long.parseLong(
                SharedPreferenceManager.getSharedPreferenceManagerInstance().getUserAllocatedStorage()), true);

        edtPrefEmail = (EditTextPreference) getPreferenceManager().findPreference("userTenantName");
        edtPrefSpaceUsed = (EditTextPreference) getPreferenceManager().findPreference("userStorageDetails");
        prefUpdgradeAccount = getPreferenceManager().findPreference("upgradeAccount");

        edtPrefEmail.setSummary(tenantName);
        edtPrefSpaceUsed.setSummary(storageUsed + " of " + allocatedStorage);
        prefUpdgradeAccount.setOnPreferenceClickListener(this);
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        startActivity(new Intent(getActivity(), UpgradeAccountActivity.class));
        return true;
    }
}
