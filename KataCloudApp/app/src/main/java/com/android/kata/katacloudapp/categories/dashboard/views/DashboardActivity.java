package com.android.kata.katacloudapp.categories.dashboard.views;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.baseactivity.BaseActivity;
import com.android.kata.katacloudapp.categories.dashboard.model.androidcomponents.intentservice.DownloadFilesIntentService;
import com.android.kata.katacloudapp.categories.dashboard.model.androidcomponents.intentservice.MiscIntentService;
import com.android.kata.katacloudapp.categories.dashboard.model.androidcomponents.intentservice.MusicIntentService;
import com.android.kata.katacloudapp.categories.dashboard.model.androidcomponents.intentservice.PhotosIntentService;
import com.android.kata.katacloudapp.categories.dashboard.model.androidcomponents.intentservice.UploadFilesIntentService;
import com.android.kata.katacloudapp.categories.dashboard.model.androidcomponents.intentservice.VideosIntentService;
import com.android.kata.katacloudapp.categories.dashboard.presenter.DashboardContract;
import com.android.kata.katacloudapp.categories.dashboard.presenter.DashboardContractImpl;
import com.android.kata.katacloudapp.categories.dashboard.views.adapter.ViewPagerAdapter;
import com.android.kata.katacloudapp.categories.dashboard.views.miscs.MiscFragment;
import com.android.kata.katacloudapp.categories.dashboard.views.musics.MusicFragment;
import com.android.kata.katacloudapp.categories.dashboard.views.photos.PhotosFragment;
import com.android.kata.katacloudapp.categories.dashboard.views.videos.VideosFragment;
import com.android.kata.katacloudapp.categories.login.views.LoginActivity;
import com.android.kata.katacloudapp.categories.search.views.SearchActivity;
import com.android.kata.katacloudapp.categories.settings.model.androidcomponents.intentservice.SettingsIntentService;
import com.android.kata.katacloudapp.categories.settings.views.SettingsActivity;
import com.android.kata.katacloudapp.utilities.checkers.CheckersUtils;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.database.greendao.db.GreenDaoManager;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;
import com.cocosw.bottomsheet.BottomSheet;

public class DashboardActivity extends BaseActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener,
        DashboardContract.DashboardView {

    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    /**
     * Initializing Fragments
     */
    PhotosFragment photosFragment = new PhotosFragment();
    VideosFragment videosFragment = new VideosFragment();
    MusicFragment musicFragment = new MusicFragment();
    MiscFragment miscFragment = new MiscFragment();

    /**
     * Declaring Views
     */
    CoordinatorLayout coordinatorLayout;
    FloatingActionButton fab;
    Toolbar toolbar;
    TextView txtUserFirstname, txtUserEmail;
    String strUserEmail, strUserFirstname;
    DrawerLayout drawer;
    NavigationView navigationView;
    TabLayout viewPagerTabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    boolean isPhotoSynchronized = false;
    boolean isVideoSynchronized = false;
    boolean isMusicSynchronized = false;
    boolean isMiscSynchronized = false;


    /**
     * Broadcast Receiver
     */
    private BroadcastReceiver broadcastReceiver;
    IntentFilter intentFilter;

    /**
     * Bottom Sheet
     */
    BottomSheet bottomSheet;

    /**
     * Presenter Interface
     */
    DashboardContract.DashboardPresenter dashboardPresenter;

    /**
     * Alert Dialog
     */
    AlertDialog alertDialog;

    /**
     * File Viewer
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        /**Initialize Singleton class*/
        GreenDaoManager.init(this);
        SharedPreferenceManager.init(this);
        CheckersUtils.init();

        dashboardPresenter = new DashboardContractImpl(this);

        setupBroadcastReceiver();
        setupViews();
        setupDrawerDetails();

        /**Setup Android Service*/
        syncAllContainerData(4);
    }

    private void syncAllContainerData(int page_position) {
        GreenDaoManager.getGreenDaoManagerInstance().deleteAllFileItems();
        isPhotoSynchronized = isVideoSynchronized = isMusicSynchronized = isMiscSynchronized = false;

        switch (page_position) {
            case 0:
                startService(new Intent(this, PhotosIntentService.class));
                break;

            case 1:
                startService(new Intent(this, VideosIntentService.class));
                break;

            case 2:
                startService(new Intent(this, MusicIntentService.class));
                break;

            case 3:
                startService(new Intent(this, MiscIntentService.class));
                break;

            default:
                startService(new Intent(this, PhotosIntentService.class));
                startService(new Intent(this, VideosIntentService.class));
                startService(new Intent(this, MusicIntentService.class));
                startService(new Intent(this, MiscIntentService.class));
                startService(new Intent(this, SettingsIntentService.class));
                break;
        }
    }

    private void setupBroadcastReceiver() {
        intentFilter = new IntentFilter(Constants.SYNCHRONIZATION_RECEIVER);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getStringExtra(Constants.INTENT_ACTIONTAG_KEY);
                String container = intent.getStringExtra(Constants.INTENT_CONTAINER_KEY);

                switch (action) {
                    case Constants.ACTION_SYNCHRONIZE_TAG:
                        switch (container) {
                            case Constants.CONTAINER_PHOTOS_TAG:
                                photosFragment.loadAllPhotoFiles();
                                isPhotoSynchronized = true;
                                break;

                            case Constants.CONTAINER_VIDEOS_TAG:
                                videosFragment.loadAllVideoFiles();
                                isVideoSynchronized = true;
                                break;

                            case Constants.CONTAINER_MUSIC_TAG:
                                musicFragment.loadAllMusicFiles();
                                isMusicSynchronized = true;
                                break;

                            case Constants.CONTAINER_MISC_TAG:
                                miscFragment.loadAllMiscFiles();
                                isMiscSynchronized = true;
                                break;
                        }

                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "Synchronizing " + container + " completed",
                                        Snackbar.LENGTH_LONG);
                        snackbar.show();
                        break;

                    case Constants.ACTION_DOWNLOAD_TAG:

                        break;
                }
            }
        };
        registerReceiver(broadcastReceiver, intentFilter);
    }

    private void setupViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /**Setting up coordinator layout*/
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        /**Setting up floating button*/
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        /**Setting up Drawer Layout and Drawer toggle*/
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        /**Setting up Navigation view and navigation header views*/
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        txtUserFirstname = (TextView) navigationView.getHeaderView(0).findViewById(R.id.txtUserFirstname);
        txtUserEmail = (TextView) navigationView.getHeaderView(0).findViewById(R.id.txtUserEmail);

        /**Setting up View Pager Views*/
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(photosFragment, Constants.CONTAINER_PHOTOS_TAG);
        viewPagerAdapter.addFragment(videosFragment, Constants.CONTAINER_VIDEOS_TAG);
        viewPagerAdapter.addFragment(musicFragment, Constants.CONTAINER_MUSIC_TAG);
        viewPagerAdapter.addFragment(miscFragment, Constants.CONTAINER_MISC_TAG);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(3);
        viewPagerTabLayout = (TabLayout) findViewById(R.id.viewPagerTabLayout);
        viewPagerTabLayout.setupWithViewPager(viewPager);
    }

    private void setupDrawerDetails() {
        /**Displaying navigation header details from shared preferences*/
        strUserEmail = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTenantNameFromShared();
        strUserFirstname = SharedPreferenceManager.getSharedPreferenceManagerInstance().getUserFirstnameFromShared();
        txtUserFirstname.setText(strUserFirstname);
        txtUserEmail.setText(strUserEmail);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            onContainerBackPressedListener();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_overflow_icons, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.overflow_icon_search:
                startActivity(new Intent(this, SearchActivity.class));
                break;

            case R.id.overflow_icon_sync:
                syncAllContainerData(viewPager.getCurrentItem());
                break;

            case R.id.overflow_icon_logout:
                SharedPreferenceManager.getSharedPreferenceManagerInstance().clearAllDataFromShared();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_drawer_photos:
                viewPager.setCurrentItem(0);
                break;

            case R.id.nav_drawer_videos:
                viewPager.setCurrentItem(1);
                break;

            case R.id.nav_drawer_music:
                viewPager.setCurrentItem(2);
                break;

            case R.id.nav_drawer_misc:
                viewPager.setCurrentItem(3);
                break;

            case R.id.nav_settings:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(DashboardActivity.this, SettingsActivity.class));
                    }
                }, 500);

                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                switch (viewPager.getCurrentItem()) {
                    case 0:
                        photosFragment.onFloatingButtonPressed();
                        break;

                    case 1:
                        videosFragment.onFloatingButtonPressed();
                        break;

                    case 2:
                        musicFragment.onFloatingButtonPressed();
                        break;

                    case 3:
                        miscFragment.onFloatingButtonPressed();
                        break;
                }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onFolderSelectedListener(String filePath, int depth, String container) {
        switch (container) {
            case Constants.CONTAINER_PHOTOS_TAG:
                photosFragment.onFolderSelected(filePath, depth);
                break;

            case Constants.CONTAINER_VIDEOS_TAG:
                videosFragment.onFolderSelected(filePath, depth);
                break;

            case Constants.CONTAINER_MUSIC_TAG:
                musicFragment.onFolderSelected(filePath, depth);
                break;

            case Constants.CONTAINER_MISC_TAG:
                miscFragment.onFolderSelected(filePath, depth);
                break;

        }
    }

    @Override
    public void onFileSelectedListener(String container, int position) {
        switch (container) {
            case Constants.CONTAINER_PHOTOS_TAG:
                photosFragment.onFileSelected(position);
                break;

            case Constants.CONTAINER_VIDEOS_TAG:
                videosFragment.onFileSelected(position);
                break;

            case Constants.CONTAINER_MUSIC_TAG:
                musicFragment.onFileSelected(position);
                break;

            case Constants.CONTAINER_MISC_TAG:
                miscFragment.onFileSelected(position);
                break;
        }
    }

    @Override
    public void onContainerBackPressedListener() {
        switch (viewPager.getCurrentItem()) {
            case 0:
                photosFragment.onBackPressed();
                break;

            case 1:
                videosFragment.onBackPressed();
                break;

            case 2:
                musicFragment.onBackPressed();
                break;

            case 3:
                miscFragment.onBackPressed();
                break;
        }
    }

    /**
     * BottomSheet event listeners for server calls
     */
    @Override
    public void onBottomSheetListener(final String filepath, final int layout, final String container) {
        bottomSheet = new BottomSheet.Builder(this)
                .title(filepath)
                .sheet(layout)
                .listener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            /**onClick in Floating Action Bar*/
                            case R.id.menu_create:
                                showCreateRenameDialog(filepath, container, Constants.ACTION_CREATE_TAG);
                                break;

                            case R.id.menu_upload:
                                /**Open file chooser*/
                                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                                intent.setType("*/*");
                                intent.addCategory(Intent.CATEGORY_OPENABLE);
                                startActivityForResult(Intent.createChooser(intent, "Select Files"),
                                        Constants.FILE_CHOOSER_REQUESTCODE);

                                break;

                            /**onLongPressed in Container Adapter*/
                            case R.id.menu_copy:

                                break;

                            case R.id.menu_delete:
                                dashboardPresenter.onDeleteListener(DashboardActivity.this, filepath, container, Constants.ACTION_DELETE_TAG);
                                break;

                            case R.id.menu_rename:
                                showCreateRenameDialog(filepath, container, Constants.ACTION_RENAME_TAG);
                                break;

                            case R.id.menu_move:

                                break;

                            case R.id.menu_dowload:
                                /**Call download intent service for downloading file from server*/
                                String fileObjectName = CheckersUtils.getCheckersUtilsInstance().getFileItemObjectName(filepath);
                                startService(new Intent(DashboardActivity.this, DownloadFilesIntentService.class)
                                        .putExtra(Constants.INTENT_ACTIONTAG_KEY, Constants.ACTION_DOWNLOAD_TAG)
                                        .putExtra(Constants.INTENT_FILEPATH_KEY, filepath)
                                        .putExtra(Constants.INTENT_FILEOBJECTNAME_KEY, fileObjectName)
                                        .putExtra(Constants.INTENT_CONTAINER_KEY, container));

                                break;

                        }
                    }
                })
                .build();
        bottomSheet.show();
    }

    @Override
    public void onUploadButtonPressedListener(Intent data, String filepath, String container) {
        Uri uri = data.getData();
//        String[] filePathColumn = {MediaStore.Files.FileColumns.DATA};
//        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
//        Log.e(TAG, "onUploadButtonPressedListener: " + cursor);
//        if (cursor != null) {
//            cursor.moveToFirst();
//            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//
//            String actualFilePath = cursor.getString(columnIndex);
//            Log.e(TAG, "onActivityResult: " + actualFilePath);
//
//            cursor.close();

        /**Call upload intent service for uploading file to server*/
        startService(new Intent(DashboardActivity.this, UploadFilesIntentService.class)
                .putExtra(Constants.INTENT_ACTIONTAG_KEY, Constants.ACTION_UPLOAD_TAG)
                .putExtra(Constants.INTENT_FILEPATH_KEY, filepath)
                .putExtra(Constants.INTENT_ACTUAL_FILEPATH_KEY,
                        CheckersUtils.getCheckersUtilsInstance().pathGenerator(uri, this))
                .putExtra(Constants.INTENT_CONTAINER_KEY, container));
//        } else {
//            Toast.makeText(getApplicationContext(), "Please select file again",
//                    Toast.LENGTH_LONG).show();
//        }

        Log.e(TAG, "onUploadButtonPressedListener: " + CheckersUtils.getCheckersUtilsInstance()
                .pathGenerator(uri, this));
    }

    public void showCreateRenameDialog(final String filepath, final String container, final String action) {
        View view = LayoutInflater.from(this)
                .inflate(R.layout.dialog_create_rename_object, null);
        final EditText edtObjectName = (EditText) view.findViewById(R.id.edtObjectName);
        alertDialog = new AlertDialog.Builder(DashboardActivity.this)
                .setTitle(action)
                .setView(view)
                .setPositiveButton(R.string.label_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (action) {
                            case Constants.ACTION_CREATE_TAG:
                                String strObjectName = edtObjectName.getText().toString();
                                String url = filepath + strObjectName + "/";
                                dashboardPresenter.onCreateListener(DashboardActivity.this, url, container, action);
                                break;

                            case Constants.ACTION_RENAME_TAG:

                                break;
                        }
                    }
                })
                .setNegativeButton(R.string.label_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        alertDialog.dismiss();
                    }
                }).create();
        alertDialog.show();
    }

    /**
     * Create object callback from DashboardContractImpl.java
     */
    @Override
    public void onSuccessCallback(String actionTag) {
        /**Refreshing container layouts when server call is successful*/
        switch (viewPager.getCurrentItem()) {
            case 0:
                photosFragment.refreshContainerLayout();
                break;

            case 1:
                videosFragment.refreshContainerLayout();
                break;

            case 2:
                musicFragment.refreshContainerLayout();
                break;

            case 3:
                miscFragment.refreshContainerLayout();
                break;
        }
        Log.e(TAG, "onSuccessCallback: " + actionTag);
    }

    /**
     * Create object callback from DashboardContractImpl.java
     */
    @Override
    public void onErrorCallback() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Constants.FILE_CHOOSER_REQUESTCODE:
                if (resultCode == RESULT_OK) {
                    if (requestCode == Constants.FILE_CHOOSER_REQUESTCODE && data != null) {
                        switch (viewPager.getCurrentItem()) {
                            case 0:
                                photosFragment.onUploadButtonPressed(data);
                                break;

                            case 1:
                                videosFragment.onUploadButtonPressed(data);
                                break;

                            case 2:
                                musicFragment.onUploadButtonPressed(data);
                                break;

                            case 3:
                                miscFragment.onUploadButtonPressed(data);
                                break;
                        }
                    }
                }
                break;

        }
    }
}
