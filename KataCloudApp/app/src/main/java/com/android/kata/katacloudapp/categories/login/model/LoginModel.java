package com.android.kata.katacloudapp.categories.login.model;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kata on 12/1/16.
 */

public class LoginModel {


    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("tenantid")
    @Expose
    private String tenantid;
    @SerializedName("tenantname")
    @Expose
    private String tenantname;
    @SerializedName("tokenexp")
    @Expose
    private String tokenexp;
    @SerializedName("storage_size")
    @Expose
    private String storageSize;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("DateCreated")
    @Expose
    private String dateCreated;
    @SerializedName("DateLastSyncOn")
    @Expose
    private String dateLastSyncOn;
    @SerializedName("CameraStatus")
    @Expose
    private String cameraStatus;
    @SerializedName("WifiStatus")
    @Expose
    private String wifiStatus;

    /**
     * @return The token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token The token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return The tenantid
     */
    public String getTenantid() {
        return tenantid;
    }

    /**
     * @param tenantid The tenantid
     */
    public void setTenantid(String tenantid) {
        this.tenantid = tenantid;
    }

    /**
     * @return The tenantname
     */
    public String getTenantname() {
        return tenantname;
    }

    /**
     * @param tenantname The tenantname
     */
    public void setTenantname(String tenantname) {
        this.tenantname = tenantname;
    }

    /**
     * @return The tokenexp
     */
    public String getTokenexp() {
        return tokenexp;
    }

    /**
     * @param tokenexp The tokenexp
     */
    public void setTokenexp(String tokenexp) {
        this.tokenexp = tokenexp;
    }

    /**
     * @return The storageSize
     */
    public String getStorageSize() {
        return storageSize;
    }

    /**
     * @param storageSize The storage_size
     */
    public void setStorageSize(String storageSize) {
        this.storageSize = storageSize;
    }

    /**
     * @return The firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname The firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return The lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname The lastname
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return The dateCreated
     */
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated The DateCreated
     */
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return The dateLastSyncOn
     */
    public String getDateLastSyncOn() {
        return dateLastSyncOn;
    }

    /**
     * @param dateLastSyncOn The DateLastSyncOn
     */
    public void setDateLastSyncOn(String dateLastSyncOn) {
        this.dateLastSyncOn = dateLastSyncOn;
    }

    /**
     * @return The cameraStatus
     */
    public String getCameraStatus() {
        return cameraStatus;
    }

    /**
     * @param cameraStatus The CameraStatus
     */
    public void setCameraStatus(String cameraStatus) {
        this.cameraStatus = cameraStatus;
    }

    /**
     * @return The wifiStatus
     */
    public String getWifiStatus() {
        return wifiStatus;
    }

    /**
     * @param wifiStatus The WifiStatus
     */
    public void setWifiStatus(String wifiStatus) {
        this.wifiStatus = wifiStatus;
    }

    public String toString() {
        return new Gson().toJson(this);
    }
}