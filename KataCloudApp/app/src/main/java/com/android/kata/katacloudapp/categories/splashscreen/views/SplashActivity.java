package com.android.kata.katacloudapp.categories.splashscreen.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.login.views.LoginActivity;
import com.android.kata.katacloudapp.categories.registration.views.RegistrationActivity;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnLogin, btnRegistration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        setupViews();
    }

    private void setupViews() {
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegistration = (Button) findViewById(R.id.btnRegistration);
        btnLogin.setOnClickListener(this);
        btnRegistration.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                startActivity(new Intent(this, LoginActivity.class));
                break;

            case R.id.btnRegistration:
                startActivity(new Intent(this, RegistrationActivity.class));
                break;
        }
    }
}
