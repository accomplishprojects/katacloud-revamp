package com.android.kata.katacloudapp.categories.search.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.search.presenter.SearchContract;
import com.android.kata.katacloudapp.utilities.checkers.CheckersUtils;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.database.greendao.db.FileItems;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private SearchContract.SearchView searchView;
    private Context mContext;
    private List<FileItems> mFileItemsList;
    private ArrayList<FileItems> arraylist;
    ViewHolder vh;

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtObjectName;
        ImageView imgFileItem;

        public ViewHolder(View itemView) {
            super(itemView);
            txtObjectName = (TextView) itemView.findViewById(R.id.txtObjectName);
            imgFileItem = (ImageView) itemView.findViewById(R.id.imgFileItem);
        }
    }

    public SearchAdapter(Context context, List<FileItems> fileItemsList) {
        SharedPreferenceManager.init(context);
        CheckersUtils.init();

        searchView = (SearchContract.SearchView) context;
        mContext = context;
        mFileItemsList = fileItemsList;
        arraylist = new ArrayList<>();
        arraylist.addAll(fileItemsList);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_item_fragment_container, parent, false);

        vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        String tenantId = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTenantIdFromShared();
        String token = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTokenFromShared();

        final String strFileItemName = mFileItemsList.get(position).getName();
        final String strFileContainer = mFileItemsList.get(position).getContainer();
        String strFileObjectName = mFileItemsList.get(position).getObject_name();
        final int fileDepth = mFileItemsList.get(position).getDepth();

        holder.txtObjectName.setText(strFileObjectName);

        /**Defining URL for fetching images, videos, audio etc.*/
        final String url = Constants.API_BASE_URL_1 + "AUTH_" + tenantId + "/" + strFileContainer + "/" + strFileItemName;

        CheckersUtils.getCheckersUtilsInstance().setFileItemIcons(mContext, url, token, holder.imgFileItem);
        holder.imgFileItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckersUtils.getCheckersUtilsInstance().checkIfFolder(strFileItemName)) {
                    searchView.onFolderSelectedListener(strFileItemName, fileDepth, strFileContainer);
                } else {
                    searchView.onFileSelectedListener(strFileContainer, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFileItemsList.size();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mFileItemsList.clear();
        if (charText.length() == 0) {
            mFileItemsList.addAll(arraylist);

        } else {
            for (FileItems fileItem : arraylist) {
                if (charText.length() != 0 && fileItem.getObject_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                    mFileItemsList.add(fileItem);
                } else if (charText.length() != 0 && fileItem.getObject_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                    mFileItemsList.add(fileItem);
                }
            }
        }
        notifyDataSetChanged();
    }
}
