package com.android.kata.katacloudapp.categories.dashboard.model.web.api;

import com.android.kata.katacloudapp.utilities.database.greendao.db.FileItems;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

public interface DashboardAPIRoutes {

    /**
     * Call for synchronizing container files from server. This is implemented in IntentService classes
     */
    @GET("AUTH_{tenantId}/{container}?format=json")
    Call<List<FileItems>> onGetContainerFiles(@Header("X-Auth-Token") String token,
                                              @Path("tenantId") String tenantId,
                                              @Path("container") String container);

    /**
     * Call for creating objects to server
     */
    @PUT("AUTH_{tenantId}/{container}/{filepath}")
    Call<ResponseBody> onCreateObject(@Header("X-Auth-Token") String token,
                                      @Path("tenantId") String tenantId,
                                      @Path("container") String container,
                                      @Path("filepath") String filepath);

    @Streaming
    @GET("AUTH_{tenantId}/{container}/{filepath}")
    Call<ResponseBody> onDowloadObject(@Header("X-Auth-Token") String token,
                                       @Path("tenantId") String tenantId,
                                       @Path("container") String container,
                                       @Path("filepath") String filepath);

    /**
     * Call for deleting objects to server
     */
    @PUT("AUTH_{tenantId}/{container}/{filepath}")
    Call<ResponseBody> onDeleteObject(@Header("X-Auth-Token") String token,
                                      @Path("tenantId") String tenantId,
                                      @Path("container") String container,
                                      @Path("filepath") String filepath);

    @PUT("AUTH_{tenantId}/{container}/{filepath}")
    Call<ResponseBody> onUploadObject(@Header("X-Auth-Token") String token,
                                      @Path("tenantId") String tenantId,
                                      @Path("container") String container,
                                      @Path("filepath") String filepath,
                                      @Body RequestBody file);
}
