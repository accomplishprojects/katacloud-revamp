package com.android.kata.katacloudapp.categories.registration.model.web.service;

import android.content.Context;
import android.util.Log;

import com.android.kata.katacloudapp.categories.registration.model.RegisterModel;
import com.android.kata.katacloudapp.categories.registration.model.web.api.RegisterAPIProvider;
import com.android.kata.katacloudapp.utilities.device.DeviceManager;
import com.android.kata.katacloudapp.utilities.network.RestManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterServiceImpl implements RegisterService {

    String TAG = this.getClass().getSimpleName();

    @Override
    public void doRegister(Context context, final OnFinishedValidation listener, String email, String firsname, String lastname, String password) {
        RestManager.init();
        DeviceManager.init(context);

        String imei = DeviceManager.getDeviceManagerInstance().getImei();
        String model = DeviceManager.getDeviceManagerInstance().getModel();
        String serial = "";
        String autoupload = "true";

        Call<RegisterModel> registerCall = RegisterAPIProvider.getRegisterAPIRoutes()
                .onRegister(email, firsname, lastname, password, imei, model, serial, autoupload);
        registerCall.enqueue(new Callback<RegisterModel>() {
            @Override
            public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                Log.e(TAG, "onResponse: " + response.body().toString());
                listener.onSuccess();
            }

            @Override
            public void onFailure(Call<RegisterModel> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                listener.onError();
            }
        });
    }
}
