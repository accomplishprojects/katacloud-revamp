package com.android.kata.katacloudapp.categories.baseactivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.android.kata.katacloudapp.R;
import com.tsengvn.typekit.TypekitContextWrapper;

public class BaseActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        // Activate StrictMode
//        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//                .detectAll()
//                .detectDiskReads()
//                .detectDiskWrites()
//                .detectNetwork()
//                // alternatively .detectAll() for all detectable problems
//                .penaltyLog()
//                .penaltyDeath()
//                .build());
//
//        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//                .detectLeakedSqlLiteObjects()
//                .detectLeakedClosableObjects()
//                // alternatively .detectAll() for all detectable problems
//                .penaltyLog()
//                .penaltyDeath()
//                .build());

        mProgressDialog = new ProgressDialog(this);
    }

    /**
     * Attach default font type for the app
     */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    public void showProgressDialog() {
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setProgressStyle(R.style.CustomProgressDialogTheme);
        mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mProgressDialog.show();
        mProgressDialog.setContentView(R.layout.dialog_custom_progress_loader);
    }

    public void hideProgressDialog() {
        mProgressDialog.hide();
    }
}
