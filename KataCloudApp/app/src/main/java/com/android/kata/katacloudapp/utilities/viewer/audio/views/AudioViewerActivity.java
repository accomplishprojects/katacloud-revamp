package com.android.kata.katacloudapp.utilities.viewer.audio.views;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.baseactivity.BaseActivity;
import com.android.kata.katacloudapp.utilities.checkers.CheckersUtils;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.converters.DataConverters;
import com.android.kata.katacloudapp.utilities.viewer.audio.androidservice.AudioForegroundService;
import com.google.android.exoplayer.util.Util;

public class AudioViewerActivity extends BaseActivity implements View.OnClickListener {

    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    /**
     * Views
     */
    Toolbar toolbar;
    TextView txtTitle, txtPlayerCurrentPosition, txtPlayerDuration;
    ImageView imgPlay, imgPause, imgRewind, imgFastForward;
    SeekBar seekBar;


    /**
     * Variables
     */
    private String fileItemName;
    private String fileItemObjectName;
    private String container;
    IntentFilter intentFilter;
    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_viewer);
        CheckersUtils.init();
        setupViews();
        getDataViaIntent();
        startAudioForegroundService();
        setupBroadcastReceiver();

    }

    private void setupViews() {
        /**Setting up views*/
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtTitle = (TextView) findViewById(R.id.txtFileTitle);
        txtPlayerCurrentPosition = (TextView) findViewById(R.id.txtPlayerCurrentPosition);
        txtPlayerDuration = (TextView) findViewById(R.id.txtPlayerDuration);

        imgPlay = (ImageView) findViewById(R.id.imgPlay);
        imgPlay.setOnClickListener(this);
        imgPause = (ImageView) findViewById(R.id.imgPause);
        imgPause.setOnClickListener(this);
        imgRewind = (ImageView) findViewById(R.id.imgRewind);
        imgRewind.setOnClickListener(this);
        imgFastForward = (ImageView) findViewById(R.id.imgFastForward);
        imgFastForward.setOnClickListener(this);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
    }

    private void getDataViaIntent() {
        fileItemName = getIntent().getStringExtra(Constants.INTENT_FILEPATH_KEY);
        fileItemObjectName = getIntent().getStringExtra(Constants.INTENT_FILEOBJECTNAME_KEY);
        container = getIntent().getStringExtra(Constants.INTENT_CONTAINER_KEY);
        txtTitle.setText(fileItemObjectName);
    }

    private void startAudioForegroundService() {
        startService(new Intent(this, AudioForegroundService.class)
                .putExtra(Constants.INTENT_FILEPATH_KEY, fileItemName)
                .putExtra(Constants.INTENT_FILEOBJECTNAME_KEY, fileItemObjectName)
                .putExtra(Constants.INTENT_CONTAINER_KEY, container)
                .putExtra(Constants.INTENT_CONTENT_TYPE_EXTRA, Util.TYPE_OTHER)
                .setAction(Constants.START_AUDIO_FOREGROUND_SERVICE));
    }

    private void setupBroadcastReceiver() {
        intentFilter = new IntentFilter(Constants.AUDIO_PLAYER_RECEIVER);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e(TAG, "onReceive: " + intent.getAction());
                String notificationAction = intent.getStringExtra(Constants.INTENT_AUDIO_NOTIFICATION_ACTION);

                switch (notificationAction) {
                    case Constants.PLAY_AUDIO_FOREGROUND_SERVICE:
                        isAudioPlaying(true);
                        break;

                    case Constants.STOP_AUDIO_FOREGROUND_SERVICE:
                        txtPlayerCurrentPosition.setText(DataConverters.convertMillisecondsToTimeFormat(0));
                        seekBar.setProgress(0);
                        isAudioPlaying(false);
                        break;

                    case Constants.PAUSE_AUDIO_FOREGROUND_SERVICE:
                        isAudioPlaying(false);
                        break;

                    case Constants.GET_AUDIO_FOREGROUND_BUFFERED_TIME:
                        long playerPosition = intent.getLongExtra(Constants.INTENT_PLAYER_BUFFERED_TIME, 0);
                        long playerDuration = intent.getLongExtra(Constants.INTENT_PLAYER_DURATION, 0);
                        int playerBufferingPosition = intent.getIntExtra(Constants.INTENT_PLAYER_BUFFERING_TIME, 0);
                        boolean playerStatus = intent.getBooleanExtra(Constants.INTENT_PLAYER_STATUS, false);
                        seekBar.setProgress((int) playerPosition);
                        seekBar.setSecondaryProgress(playerBufferingPosition);
                        seekBar.setMax((int) playerDuration);

                        isAudioPlaying(playerStatus);
                        txtPlayerCurrentPosition.setText(DataConverters.
                                convertMillisecondsToTimeFormat(playerPosition));
                        txtPlayerDuration.setText(DataConverters.
                                convertMillisecondsToTimeFormat(playerDuration));
                        break;
                }
            }
        };
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgPlay:
                if (CheckersUtils.checkIfServiceRunning(AudioForegroundService.class.getName(),
                        getApplicationContext())) {
                    doActionInService(Constants.PLAY_AUDIO_FOREGROUND_SERVICE);
                } else {
                    doActionInService(Constants.START_AUDIO_FOREGROUND_SERVICE);
                }
                break;

            case R.id.imgPause:
                doActionInService(Constants.PAUSE_AUDIO_FOREGROUND_SERVICE);
                break;

            case R.id.imgRewind:
                doActionInService(Constants.REWIND_AUDIO_FOREGROUND_SERVICE);
                break;

            case R.id.imgFastForward:
                doActionInService(Constants.FAST_FORWARD_AUDIO_FOREGROUND_SERVICE);
                break;

        }
    }

    public void doActionInService(String action) {
        startService(new Intent(this, AudioForegroundService.class)
                .putExtra(Constants.INTENT_FILEPATH_KEY, fileItemName)
                .putExtra(Constants.INTENT_FILEOBJECTNAME_KEY, fileItemObjectName)
                .putExtra(Constants.INTENT_CONTAINER_KEY, container)
                .putExtra(Constants.INTENT_CONTENT_TYPE_EXTRA, Util.TYPE_OTHER)
                .setAction(action));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        doActionInService(Constants.STOP_AUDIO_FOREGROUND_SERVICE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    private void isAudioPlaying(boolean status) {
        if (status) {
            imgPlay.setVisibility(View.GONE);
            imgPause.setVisibility(View.VISIBLE);
        } else {
            imgPlay.setVisibility(View.VISIBLE);
            imgPause.setVisibility(View.GONE);
        }
    }


}
