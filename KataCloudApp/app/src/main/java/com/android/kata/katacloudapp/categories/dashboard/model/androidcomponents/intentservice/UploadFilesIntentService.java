package com.android.kata.katacloudapp.categories.dashboard.model.androidcomponents.intentservice;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.dashboard.model.web.api.DashboardAPIProvider;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.network.RestManager;
import com.android.kata.katacloudapp.utilities.notification.CustomNotificationManager;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadFilesIntentService extends IntentService {

    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    public UploadFilesIntentService() {
        super("UploadFilesIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        RestManager.init();
        SharedPreferenceManager.init(getApplicationContext());
        CustomNotificationManager.init(getApplicationContext());

        String actionTag = intent.getStringExtra(Constants.INTENT_ACTIONTAG_KEY);
        String filepath = intent.getStringExtra(Constants.INTENT_FILEPATH_KEY);
        String actualFilePath = intent.getStringExtra(Constants.INTENT_ACTUAL_FILEPATH_KEY);
        String container = intent.getStringExtra(Constants.INTENT_CONTAINER_KEY);
        String token = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTokenFromShared();
        String tenantId = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTenantIdFromShared();

        File file = new File(actualFilePath);
        String url = filepath + file.getName();
//        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        RequestBody requestFile = RequestBody.create(MediaType.parse("application/octet-stream"), file);
//        MultipartBody.Part body = MultipartBody.Part.createFormData("uploadedfile", file.getName(), requestFile);

        Log.e(TAG, "onHandleIntent: ActionTag: " + actionTag +
                " Filepath: " + filepath +
                " ActualFilePath: " + actualFilePath +
                " Container: " + container +
                " FileObjectName: " + file.getName());

        /**Show Notification*/
        String title = getString(R.string.app_name);
        String message = "Uploading " + file.getName();
        CustomNotificationManager.getsNotificationManagerInstance()
                .onShowNotification(android.R.drawable.stat_sys_upload, title,
                        message, Constants.NOTIFICATION_UPLOAD_TAG);

        Call<ResponseBody> onUploadObjectToServer = DashboardAPIProvider.getDashboardAPIRoutes()
                .onUploadObject(token, tenantId, container, url, requestFile);
        onUploadObjectToServer.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e(TAG, "onResponse: " + response.raw().toString());
                CustomNotificationManager.getsNotificationManagerInstance()
                        .onUploadCompleted(android.R.drawable.stat_sys_upload_done,
                                Constants.NOTIFICATION_UPLOAD_TAG, "File upload successful");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                CustomNotificationManager.getsNotificationManagerInstance()
                        .onUploadCompleted(android.R.drawable.stat_notify_error,
                                Constants.NOTIFICATION_UPLOAD_TAG, "File upload failed");
            }
        });

    }
}
