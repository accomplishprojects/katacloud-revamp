package com.android.kata.katacloudapp.categories.dashboard.views.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.dashboard.presenter.DashboardContract;
import com.android.kata.katacloudapp.utilities.checkers.CheckersUtils;
import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.android.kata.katacloudapp.utilities.database.greendao.db.FileItems;
import com.android.kata.katacloudapp.utilities.network.RestManager;
import com.android.kata.katacloudapp.utilities.sharedpreferences.SharedPreferenceManager;

import java.util.List;


public class ContainerAdapter extends RecyclerView.Adapter<ContainerAdapter.ViewHolder> {

    private List<FileItems> fileItemsList;
    private Context mContext;
    private ViewHolder vh;
    private DashboardContract.DashboardView dashboardView;
    private String strContainer;

    static class ViewHolder extends RecyclerView.ViewHolder {
        public View view;
        TextView txtObjectName;
        ImageView imgFileItem;

        ViewHolder(View v) {
            super(v);
            view = v;
            txtObjectName = (TextView) view.findViewById(R.id.txtObjectName);
            imgFileItem = (ImageView) view.findViewById(R.id.imgFileItem);
        }
    }

    public ContainerAdapter(Context context, List<FileItems> fileItemsList, String container) {
        SharedPreferenceManager.init(context);
        CheckersUtils.init();

        RestManager.init();
        mContext = context;
        strContainer = container;
        this.fileItemsList = fileItemsList;
        dashboardView = (DashboardContract.DashboardView) this.mContext;
    }

    @Override
    public ContainerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.grid_item_fragment_container, parent, false);
        vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        /**Get token and tenantId from Shared*/
        final String token = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTokenFromShared();
        String tenantId = SharedPreferenceManager.getSharedPreferenceManagerInstance().getTenantIdFromShared();

        FileItems fileItems = fileItemsList.get(position);
        final String strFileItemName = fileItems.getName();
        final String strFileItemObjectName = fileItems.getObject_name();
        final int strFileItemDepth = fileItems.getDepth();
        final String strFileItemContainer = fileItems.getContainer();

        /**Setting up text view*/
        holder.txtObjectName.setText(strFileItemObjectName);

        /**Defining URL for fetching images, videos, audio etc.*/
        final String url = Constants.API_BASE_URL_1 + "AUTH_" + tenantId + "/" + strContainer + "/" + strFileItemName;

        /**Setting up Default Icon*/
        CheckersUtils.getCheckersUtilsInstance().setFileItemIcons(mContext, url, token, holder.imgFileItem);

        /**On single click listener*/
        holder.imgFileItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckersUtils.getCheckersUtilsInstance().checkIfFolder(strFileItemName)) {

                    dashboardView.onFolderSelectedListener(strFileItemName, strFileItemDepth,
                            fileItemsList.get(position).getContainer());
                } else {
                    dashboardView.onFileSelectedListener(strFileItemContainer, position);
                }
            }
        });

        /**On long click listener*/
        holder.imgFileItem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (CheckersUtils.getCheckersUtilsInstance().checkIfFolder(strFileItemName)) {
                    dashboardView.onBottomSheetListener(strFileItemName,
                            R.menu.bottom_sheet_menu_adapter_for_folders, strContainer);
                } else {
                    dashboardView.onBottomSheetListener(strFileItemName,
                            R.menu.bottom_sheet_menu_adapter_for_files, strContainer);

                }
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.fileItemsList.size();
    }

}

