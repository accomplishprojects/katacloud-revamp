package com.android.kata.katacloudapp.categories.dashboard.presenter;

import android.content.Context;

import com.android.kata.katacloudapp.categories.dashboard.model.web.service.DashboardService;
import com.android.kata.katacloudapp.categories.dashboard.model.web.service.DashboardServiceImpl;

public class DashboardContractImpl implements DashboardContract.DashboardPresenter,
        DashboardService.OnErrorResponse, DashboardService.OnSuccessResponse {

    private String TAG = "Luigi";
    private DashboardContract.DashboardView mDashboardView;
    private DashboardService dashboardService;

    public DashboardContractImpl(DashboardContract.DashboardView view) {
        mDashboardView = view;
        dashboardService = new DashboardServiceImpl();
    }

    /**
     * Call for create object to server
     */
    @Override
    public void onCreateListener(Context context, String url, String container, String actionTag) {
        dashboardService.onCreateObjectToServer(context, url, container, actionTag, this, this);
    }

    /**
     * Call for delete object to server
     */
    @Override
    public void onDeleteListener(Context context, String filepath, String container, String actionTag) {
        dashboardService.onDeleteObjectToServer(context, filepath, container, actionTag, this, this);
    }

    /**
     * Create Response from DashboardServiceImpl.java
     */
    @Override
    public void onError() {
        mDashboardView.onErrorCallback();
    }

    /**
     * Create Response from DashboardServiceImpl.java
     */
    @Override
    public void onSuccess(String actiontag) {
        mDashboardView.onSuccessCallback(actiontag);
    }

}
