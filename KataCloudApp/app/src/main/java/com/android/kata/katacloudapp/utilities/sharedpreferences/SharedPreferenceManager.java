package com.android.kata.katacloudapp.utilities.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.kata.katacloudapp.utilities.constants.Constants;

public class SharedPreferenceManager {

    public static SharedPreferenceManager sSharedPreferenceManager;
    public static SharedPreferences mSharedPreferences;

    public static void init(Context context) {
        if (sSharedPreferenceManager == null) {
            sSharedPreferenceManager = new SharedPreferenceManager();
            mSharedPreferences = context.getSharedPreferences(Constants.SHAREDPREFERENCES_FILENAME,
                    Context.MODE_PRIVATE);
        }
    }

    public static SharedPreferenceManager getSharedPreferenceManagerInstance() {
        if (sSharedPreferenceManager != null) {
            return sSharedPreferenceManager;
        }
        throw new IllegalStateException("Call SharedPreferenceManger init()");
    }

    public void putLoginDetailsToShared(String token, String tenantId, String tenantName,
                                        String firstname, String lastname) {
        mSharedPreferences.edit().putString(Constants.SHAREDPREFERENCES_TOKEN_KEY, token)
                .putString(Constants.SHAREDPREFERENCES_TENANT_ID_KEY, tenantId)
                .putString(Constants.SHAREDPREFERENCES_USER_TENANT_NAME_KEY, tenantName)
                .putString(Constants.SHAREDPREFERENCES_USER_FIRSTNAME_KEY, firstname)
                .putString(Constants.SHAREDPREFERENCES_USER_LASTNAME_KEY, lastname)
                .apply();
    }

    public String getTokenFromShared() {
        return mSharedPreferences.getString(Constants.SHAREDPREFERENCES_TOKEN_KEY, null);
    }

    public String getTenantIdFromShared() {
        return mSharedPreferences.getString(Constants.SHAREDPREFERENCES_TENANT_ID_KEY, null);
    }

    public String getTenantNameFromShared() {
        return mSharedPreferences.getString(Constants.SHAREDPREFERENCES_USER_TENANT_NAME_KEY, null);
    }

    public String getUserFirstnameFromShared() {
        return mSharedPreferences.getString(Constants.SHAREDPREFERENCES_USER_FIRSTNAME_KEY, null);
    }

    public void putSettingsDetailsToShared(String storageUsed, String allocatedStorage) {
        mSharedPreferences.edit()
                .putString(Constants.SHAREDPREFERENCES_STORAGE_USED, storageUsed)
                .putString(Constants.SHAREDPREFERENCES_ALLOCATED_STORAGE, allocatedStorage)
                .apply();
    }

    public String getUserStorageUsed() {
        return mSharedPreferences.getString(Constants.SHAREDPREFERENCES_STORAGE_USED, null);
    }

    public String getUserAllocatedStorage() {
        return mSharedPreferences.getString(Constants.SHAREDPREFERENCES_ALLOCATED_STORAGE, null);
    }

    public void clearAllDataFromShared() {
        mSharedPreferences.edit().clear().apply();
    }

}
