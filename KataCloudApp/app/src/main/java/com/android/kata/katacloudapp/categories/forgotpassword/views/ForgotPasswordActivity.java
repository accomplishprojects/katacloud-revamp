package com.android.kata.katacloudapp.categories.forgotpassword.views;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.kata.katacloudapp.R;
import com.android.kata.katacloudapp.categories.baseactivity.BaseActivity;
import com.android.kata.katacloudapp.categories.forgotpassword.presenter.ForgotPasswordContract;
import com.android.kata.katacloudapp.categories.forgotpassword.presenter.ForgotPasswordContractImpl;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener,
        ForgotPasswordContract.ForgotPasswordView {

    ForgotPasswordContract.ForgotPasswordPresenter forgotPasswordPresenter;
    EditText edtEmail;
    Button btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        setupViews();

        forgotPasswordPresenter = new ForgotPasswordContractImpl(this);
    }

    private void setupViews() {
        /**Setting up toolbar*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        edtEmail = (EditText) findViewById(R.id.edtEmail);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSearch:
                String strEmail = edtEmail.getText().toString();
                forgotPasswordPresenter.onForgotPasswordListener(this, strEmail);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onForgotPasswordCallback() {

    }
}
