package com.android.kata.katacloudapp.utilities.network;

import com.android.kata.katacloudapp.utilities.constants.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestManager {

    private static RestManager sRestManager;
    private Retrofit sRetrofitAdapter1, sRetrofitAdapter2, sRetrofitAdapter3;
    private static OkHttpClient sOkHttpClient;
    private static Gson gson;

    public static void init() {
        if (sRestManager == null) {
            sRestManager = new RestManager();

            // Define the interceptor, add authentication headers
            Interceptor interceptor = new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    okhttp3.Request newRequest = chain.request()
                            .newBuilder()
                            .build();
                    return chain.proceed(newRequest);
                }
            };

            // Add the interceptor to OkHttpClient
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.interceptors().add(interceptor);
            sOkHttpClient = builder
                    .connectTimeout(5, TimeUnit.MINUTES)
                    .readTimeout(5, TimeUnit.MINUTES)
                    .build();
        }
    }

    public static RestManager getRestManagerInstance() {
        if (sRestManager != null) {
            return sRestManager;
        }
        throw new IllegalStateException("Call RestManger init()");
    }

    /**
     * https://proxy.kata-cloud.com:8443/v1/
     */
    public Retrofit getRetrofitAdapter1() {
        if (sRetrofitAdapter1 == null) {

            sRetrofitAdapter1 = new Retrofit.Builder()
                    .baseUrl(Constants.API_BASE_URL_1)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(sOkHttpClient)
                    .build();
        }
        return sRetrofitAdapter1;
    }

    /**
     * https://bridge.kata-cloud.com/
     */
    public Retrofit getRetrofitAdapter2() {
        if (sRetrofitAdapter2 == null) {
            sRetrofitAdapter2 = new Retrofit.Builder()
                    .baseUrl(Constants.API_BASE_URL_2)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(sOkHttpClient)
                    .build();
        }
        return sRetrofitAdapter2;
    }

    /**
     * https://storageupgrade.kata-cloud.com/
     */
    public Retrofit getRetrofitAdapter3() {
        if (sRetrofitAdapter3 == null) {
            sRetrofitAdapter3 = new Retrofit.Builder()
                    .baseUrl(Constants.API_BASE_URL_3)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(sOkHttpClient)
                    .build();
        }
        return sRetrofitAdapter3;
    }
}
