package com.android.kata.katacloudapp.categories.upgradeaccount.presenter;

import android.content.Context;

import com.android.kata.katacloudapp.categories.upgradeaccount.model.web.service.UpgradeAccountService;
import com.android.kata.katacloudapp.categories.upgradeaccount.model.web.service.UpgradeAccountServiceImpl;

public class UpgradeAccountContractImpl implements UpgradeAccountContract.UpgradeAccountPresenter,
        UpgradeAccountService.OnSuccessResponse, UpgradeAccountService.OnErrorResponse {

    UpgradeAccountContract.UpgradeAccountView upgradeAccountView;
    UpgradeAccountService upgradeAccountService;

    public UpgradeAccountContractImpl(UpgradeAccountContract.UpgradeAccountView view) {
        upgradeAccountView = view;
        upgradeAccountService = new UpgradeAccountServiceImpl();
    }

    @Override
    public void onValidateCodeListener(Context context, String voucherCode) {
        upgradeAccountService.onValidateVoucherCode(context, voucherCode, this, this);
    }

    @Override
    public void onSubscribeListener(Context context) {
        upgradeAccountService.onSubscribe(context, this, this);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onSuccess() {

    }
}
