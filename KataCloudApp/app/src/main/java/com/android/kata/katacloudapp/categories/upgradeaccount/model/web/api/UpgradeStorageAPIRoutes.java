package com.android.kata.katacloudapp.categories.upgradeaccount.model.web.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface UpgradeStorageAPIRoutes {

    @FormUrlEncoded
    @POST("KataCloud/ValidateCode.php")
    Call<ResponseBody> onValidateVoucherCode(@Header("X-Auth-Token") String token,
                                             @Field("email") String email,
                                             @Field("requestCode") String requestCode,
                                             @Field("voucherCode") String voucherCode,
                                             @Field("phoneIp") String phoneIp,
                                             @Field("storageSize") String storageSize);

    @FormUrlEncoded
    @POST("KataCloud/StorageUpgrade.php")
    Call<ResponseBody> onUpgradeStorage(@Field("email") String email,
                                        @Field("requestCode") String requestCode,
                                        @Field("voucherCode") String voucherCode,
                                        @Field("phoneIp") String phoneIp,
                                        @Field("storageSize") String storageSize,
                                        @Field("imei") String imei,
                                        @Field("brand") String brand,
                                        @Field("subsDate") String subsDate,
                                        @Field("model") String model);

}
