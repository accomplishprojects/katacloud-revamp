package com.android.kata.katacloudapp.categories.forgotpassword.model.web.api;

import com.android.kata.katacloudapp.utilities.network.RestManager;

import retrofit2.Retrofit;

public class ForgotPasswordAPIProvider {

    private static Retrofit mRetrofit;

    public static ForgotPasswordAPIRoutes getForgotPasswordAPIRoutes() {
        mRetrofit = RestManager.getRestManagerInstance().getRetrofitAdapter2();
        return mRetrofit.create(ForgotPasswordAPIRoutes.class);
    }
}
