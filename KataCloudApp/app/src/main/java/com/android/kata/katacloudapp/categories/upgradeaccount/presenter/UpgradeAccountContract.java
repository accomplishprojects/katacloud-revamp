package com.android.kata.katacloudapp.categories.upgradeaccount.presenter;

import android.content.Context;

public interface UpgradeAccountContract {

    interface UpgradeAccountPresenter {
        void onValidateCodeListener(Context context, String voucherCode);
        void onSubscribeListener(Context context);
    }

    interface UpgradeAccountView {
        void onSuccessCallback();
        void onErrorCallback();

    }
}
