package com.example;


import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class GreenDaoGenerator {

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, "com.android.kata.katacloudapp.utilities.database.greendao.db");

        createFileItemsEntity(schema);

        new DaoGenerator().generateAll(schema, "./app/src/main/java");
    }

    private static Entity createFileItemsEntity(final Schema schema) {
        Entity fileItemsEntity = schema.addEntity("FileItems");
        fileItemsEntity.addIdProperty().primaryKey().autoincrement();
        fileItemsEntity.addStringProperty("tenant_id").notNull();
        fileItemsEntity.addStringProperty("hash_code").notNull();
        fileItemsEntity.addStringProperty("name").notNull();
        fileItemsEntity.addStringProperty("object_name").notNull();
        fileItemsEntity.addStringProperty("bytes").notNull();
        fileItemsEntity.addStringProperty("container").notNull();
        fileItemsEntity.addIntProperty("depth").notNull();
        fileItemsEntity.addStringProperty("last_modified");
        fileItemsEntity.addStringProperty("content_type");
        return fileItemsEntity;
    }
}
